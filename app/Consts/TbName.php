<?php
namespace App\Consts;


class TbName extends Consts
{
    //for leads table
    const USER_TB = 'admin_user';
    const INGREDIENT_TB = 'ingredients';
    const DICT_TB = 'drop_down_lists';
    const ALIAS_TB = 'ingredients_alias';
    const VENDOR_TB = 'vendors';
    const ADDRESS_TB = 'addresses';
    const VENDOR_INGREDIENT_DOC_TB = 'vendor_ingredients_docs';
    const VENDOR_QUOTE_TB = 'vendor_quotes';
    const QUOTE_DOCS = 'quote_docs';
    const VENDOR_CONTACT_TB = 'contacts';

    //for views
    const PACKAGE_VIEW = 'v_packaging';
}