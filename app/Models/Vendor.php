<?php

namespace App\Models;

use App\Consts\TbName;

/**
 * Class Vendor
 * @package App\Models
 */
class Vendor extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::VENDOR_TB;


    /**
     * @var string
     */
    public $primaryKey = 'vendor_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_name', 'chempax_ref', 'vender_type', 'created_by', 'created_date', 'updated_by', 'updated_date',
        'fd_member', 'website', 'status', 'assigned_to', 'qualified_id', 'address1', 'address2', 'city', 'state',
        'country', 'phone', 'first_name', 'last_name', 'email', 'payment_term', 'notes', 'country'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date', 'updated_date'
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'vendor_ingredients', 'vendor_id', 'ingredient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Addresses()
    {
        return $this->hasMany(Address::class, 'vendor_id', 'vendor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Contacts()
    {
        return $this->hasMany(Contact::class, 'vendor_id', 'vendor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function VendorIngredientDocs()
    {
        return $this->hasMany(VendorIngredientDoc::class, 'vendor_id', 'vendor_id');
    }

    /**
     * Vendor sort
     *
     * @param $query
     * @param $sortBy
     * @return mixed
     */
    public function scopeSort($query, $sortBy)
    {
        if(empty($sortBy)) {
            return $query;
        }

        list($sortName, $direction) = explode('-', $sortBy);
        if(empty($sortName) || empty($direction)) return $query;
        $query->orderBy($sortName, $direction);

        return $query;
    }

    /**
     * Vendor filter search
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeSearch($query, $filter)
    {
        if (empty($filter)) {
            return $query;
        }

        foreach($filter as $key => $value) {
            if (empty(trim($value))) {
                continue;
            }
            $value = trim($value);

            if($key=='vendor_name') {
                $query->where(function($query) use($value){
                    $query->where($this->getTable().'.vendor_name', 'like', $value.'%');
                });
            }
            if($key=='website') {
                $query->where($this->getTable().'.website', 'like', $value.'%');
            }
            if($key=='chempax_ref') {
                $query->where($this->getTable().'.chempax_ref', 'like', $value.'%');
            }
        }

        return $query;
    }
}
