<div class="navbar navbar-default" id="navbar">
            <script type="text/javascript">
                try{ace.settings.check('navbar' , 'fixed')}catch(e){}
            </script>
            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a href="{{ URL::to('/') }}" class="navbar-brand">
                        <img src={{asset("/images/logo.png")}} alt="logo" class="navbar-header-img">
                        <span class="nav-header-text">
                            Vendors Management System
                        </span>
                    </a><!-- /.brand -->
                </div>

                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">

                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <span class="user-info">
                                    <small>{{ trans('templates.home_page.welcome') }}</small>
                                    {{ Auth::user()->fullname }}
                                </span>

                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="{{ route('admin.user.profile') }}">
                                        <i class="icon-user"></i>
                                        My Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.login.sign_out') }}">
                                        <i class="icon-off"></i>
                                        {{ trans('templates.header.sign_out') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul><!-- /.ace-nav -->
                </div><!-- /.navbar-header -->
            </div>
</div>
