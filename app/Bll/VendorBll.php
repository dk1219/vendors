<?php
/**
 * Vendor Business Logic Layer
 * User: kongdaniel
 * Date: 12/19/15
 * Time: 6:41 PM
 */

namespace App\Bll;


class VendorBll extends BaseBll
{
    /**
     * @param $filter
     * @return bool
     */
    public static function isMoreFilter($filter)
    {
        $isMoreFilter = false;
        if( !empty($filter['website']) || !empty($filter['address']) || !empty($filter['city'])
            || !empty($filter['state']) || !empty($filter['zip']) || !empty($filter['country'])
            || !empty($filter['highlights']) || !empty($filter['chempax_ref'])
            || !empty($filter['qualifier']) || !empty($filter['salesrep'])) {
            $isMoreFilter = true;
        }

        return $isMoreFilter;
    }
}