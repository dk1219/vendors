@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="inv_lead_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        @include('admin.vendor._nav')

        <div class="header green col-sm-12">
            <h3 class="smaller lighter col-sm-9 lead-title">Vendor Address List</h3>
            <div class="blue col-sm-3 lead-total">Total Records: <span>{{$addresses->count()}}</span></div>
        </div>

        @include('admin.address._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th id="vendor_cn_title">Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Country</th>
                    <th>Zip</th>
                    <th>Default Address</th>
                    <th>{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$addresses->isEmpty())
                    @foreach ($addresses as $key => $address)
                        <tr >
                            <td class="center">
                                <span class="lbl">{{$key+1}}</span>
                            </td>
                            <td>
                                {{ $address->address1 }}
                            </td>
                            <td>
                                {{ $address->city }}
                            </td>
                            <td>
                                {{ $address->state }}
                            </td>
                            <td>
                                {{ $address->country }}
                            </td>
                            <td>
                                {{ $address->zip}}
                            </td>
                            <td>
                                {{ $address->is_default ? 'YES' : 'NO'}}
                            </td>
                            <td style="width: 150px;">
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.vendor-address.edit', $address) }}">
                                        <i class="icon-edit bigger-120"></i>
                                    </a>
                                    @if(!$address->is_default)
                                        <a class="btn btn-xs btn-danger delete-model" data-url="{{route('admin.vendor-address.destroy', $address)}}">
                                            <i class="icon-trash bigger-120"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

@stop