@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="inv_lead_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        <div class="header green col-sm-12" style="margin-top: 0;">
            <h3 class="smaller lighter col-sm-9 lead-title">Users List</h3>
            <div class="blue col-sm-3 lead-total">Total Users: <span>{{$users->total()}}</span></div>
        </div>

        @include('admin.user._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl">No.</span>
                        </label>
                    </th>
                    <th>Full Name</th>
                    <th>User ID</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th>Access Vendors DB</th>
                    <th>{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$users->isEmpty())
                    @foreach ($users as $key => $user)
                        <tr >
                            <td class="center">
                                {{ $key + 1 }}
                            </td>
                            <td>
                                {{ $user->fullname }}
                            </td>
                            <td>
                                {{ $user->user_id }}
                            </td>
                            <td>
                                {{ $user->role }}
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                {{ $user->access_vendorsdb ? 'YES' : 'NO'}}
                            </td>
                            <td style="width: 100px;">
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.user.edit', $user) }}">
                                        <i class="icon-edit bigger-120"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="row" style="margin-top: 30px;">
                <div class="col-sm-2 text-center">
                </div>
                <div class="col-sm-10">
                    <div class="dataTables_paginate paging_bootstrap">
                        {!! $users->appends(['filter'=>$filter])->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

    </div><!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
@stop