@if ( Session::has('fails') && !empty(Session::get('fails')) )
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
                <i class="icon-remove"></i>
        </button>
        <strong>
            {{ Session::get('fails') }}
        </strong>
    </div>
@endif

