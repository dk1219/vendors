<?php

namespace App\Http\Controllers\Admin;

use App\Bll\DictBll;
use App\Bll\UserBll;
use App\Http\Controllers\BaseController;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends BaseController
{
    /**
     * @var User
     */
    protected $user;

    /**
     * UserController constructor.
     * @param $user
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
        $this->sidebar('users');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('filter')) {
            $filter = $request->get('filter');
            Session::put('filter.lead', $filter);
        }elseif(Session::has('filter.lead')){
            $filter = Session::get('filter.lead');
        }else{
            $filter = null;
        }

        $users = $this->user->search($filter)
            ->paginate(config('view.paginations.admin'));

        return view('admin.user.userList', compact('users', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleMenu = DictBll::getRoleMenu();
        $qualifierType = DictBll::getQualifierType();

        return view('admin.user.user', compact('roleMenu', 'qualifierType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        try{
            foreach ($request->only('fullname', 'email', 'user_id', 'role', 'qualifier_type', 'access_vendorsdb') as $key => $value) {
                $this->user->$key = trim($value);
            }
            $this->user->user_password = bcrypt( $request->get('password') );
            $this->user->save();

        }catch(\Exception $e){
            Log::error($e->getMessage());
            return redirect()->route('admin.user.create')
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'User'
                        ])
                );
        }

        return redirect()->route('admin.user.index')
            ->with('success',
                trans('notifications.crud.success.create',
                    [
                        'model' => 'User'
                    ])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function edit($userId)
    {
        if(empty($userId)) abort(404);

        try{
            $user = $this->user->findOrFail($userId);
        }catch(\Exception $e){
            Log::error($e->getMessage());
            abort(404);
        }

        $roleMenu = DictBll::getRoleMenu();
        $qualifierType = DictBll::getQualifierType();

        return view('admin.user.user',
            compact('user', 'userId', 'roleMenu', 'qualifierType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, $userId)
    {
        $user = $this->user->findOrFail($userId);
        try{
            foreach ($request->only('fullname', 'email', 'user_id', 'role', 'access_vendorsdb') as $key => $value) {
                $user->$key = trim($value);
            }
            if(!empty($request->get('password'))){
                $user->user_password = bcrypt( $request->get('password') );
            }
            $user->save();
        }catch(\Exception $e){
            Log::error($e->getMessage());
            return redirect()->route('admin.user.edit', $userId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'User'
                        ])
                );
        }

        return redirect()->route('admin.user.index')
            ->with('success',
                trans('notifications.crud.success.update',
                    [
                        'model' => 'User'
                    ])
            );
    }

    /**
     * Get User List
     * @return mixed
     */
    public function userList()
    {
        $users = UserBll::getUserList();
        return $users;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        try{
            $user = Auth::User();
        }catch(\Exception $e){
            Log::error($e->getMessage());
            abort(404);
        }

        return view('admin.user.profile', compact('user'));
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(UserRequest $request)
    {
        $user = $this->user->findOrFail(Auth::User()->admin_user_id);

        try{
            if( $request->get('reset_password') && !empty($request->get('new_password')) ) {
                $user->user_password = bcrypt($request->get('new_password'));
            }
            foreach ($request->only('fullname', 'email') as $key => $value) {
                $user->$key = trim($value);
            }
            $user->save();

            Auth::setUser($user);
        }catch(\Exception $e){
            Log::error($e->getMessage());
            return redirect()->route('admin.user.profile')
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Profile'
                        ])
                );
        }

        return redirect()->route('admin.user.profile')
            ->with('success',
                trans('notifications.crud.success.update',
                    [
                        'model' => 'Profile'
                    ])
            );

    }
}
