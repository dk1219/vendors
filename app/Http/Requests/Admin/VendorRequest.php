<?php

namespace App\Http\Requests\Admin;

use App\Consts\TbName;
use App\Http\Requests\Request;

class VendorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "vendor_name" => "required|unique:".TbName::VENDOR_TB.",vendor_name,NULL,vendor_id,active,1",
            "chempax_ref" => 'digits:5',
            'website' => 'url'
        ];

        return $rules;
    }
}
