<?php

namespace App\Console\Commands;

use App\Consts\InvSyncTaskStatus;
use App\Consts\System;
use App\Consts\TbName;
use App\Models\InvSyncTask;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class HandleInvSyncTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inv-sync-task:handle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle Inv Sync Tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invSyncTasks = InvSyncTask::where('status', InvSyncTaskStatus::PENDING)->get();
        if(empty($invSyncTasks)) return;
        $uniqueInvSyncTasks = collect([]);
        foreach( $invSyncTasks->groupBy('whs') as $warehouseTasks ) {
            $uniqueInvSyncTasks = $uniqueInvSyncTasks->merge( $warehouseTasks->unique('sku') );
        }

        $client = new Client();

        foreach( $uniqueInvSyncTasks as $invSyncTask ) {
            $hasError = false;
            $res = $client->request('GET', System::INV_SYNC_API, [
                'query' => [
                    'sku' => $invSyncTask->sku,
                    'whs' => $invSyncTask->whs,
                ]
            ]);
            $statusCode = $res->getStatusCode();
            if(200!=$statusCode) {
                $hasError = true;
                $resMsg = 'API Server Error';
            }else {
                $resMsg = $res->getBody()->getContents();
            }

            //check if have error
            if(!$hasError) $hasError = str_contains($resMsg, 'err-msg');

            if($hasError) {
                $status = InvSyncTaskStatus::ERROR;
                //send email to IT
                $template = 'emails.system.inv_sync_error';
                $params = [
                    'sku' => $invSyncTask->sku,
                    'warehouseId' => $invSyncTask->whs
                ];
                $subject = 'Inventory Sync Error';
                $email = env('IT_DEPT_EMAIL', '');
                Mail::queueOn('emails', $template, $params, function($mail) use($email, $subject) {
                    $mail->to($email)->subject($subject);
                });
            }else{
                $status = InvSyncTaskStatus::FINISHED;
            }

            //update task status in db
            DB::table(TbName::INV_SYNC_TASK_TABLE)->where('sku', $invSyncTask->sku)
                ->where('whs', $invSyncTask->whs)
                ->update(['status'=>$status, 'result'=>$resMsg, 'updated_at'=>Carbon::now()]);
        }
    }
}
