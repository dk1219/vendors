<div class="page-header col-sm-12" style="margin-top:19px;">
    {!! Form::open(['route' => ['admin.vendor-ingredient-doc.index'] ,'method' => 'GET', 'id' => 'vendor_ingredient_doc_search_form']) !!}
        <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
            @if(!$ingredients->isEmpty())
                {!! Form::select('filter[ingredient_id]', $ingredients, isset($filter['ingredient_id']) ? $filter['ingredient_id'] : '', ['class' => 'search-query pull-left col-xs-5', 'placeholder'=>'Search Ingredient Name']) !!}
            @endif

            <span class="pull-right" style="margin-top: -3px;">
                @if(!$ingredients->isEmpty())
                    <input type="hidden" name="vendor_id" value="{{$vendorId}}">
                    <button type="submit" class="btn btn-purple btn-sm">
                        <i class="icon-search icon-on-right bigger-110"></i>
                        {{ trans('templates.global.search') }}
                    </button>
                @endif
                <a class="btn btn-success btn-sm" href="{{route('admin.vendor-ingredient-doc.create', ['vendor_id'=>$vendorId])}}">
                    <i class="fa fa-file-o" aria-hidden="true"></i>
                    Add Document
                </a>
            </span>
        </div>
    {!! Form::close() !!}
</div>
