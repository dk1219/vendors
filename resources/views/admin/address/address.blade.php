@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        @include('admin.vendor._nav')

        <div class="widget-box transparent">
            @include('admin.address._addressForm')
        </div>
    </div>
@stop