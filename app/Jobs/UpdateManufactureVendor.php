<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Vendor;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateManufactureVendor extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $vendor;

    /**
     * UpdateManufactureVendor constructor.
     * @param $vendor
     */
    public function __construct($vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            if(isset($this->vendor) && isset($this->vendor->vendor_name) && !empty($this->vendor->vendor_name)) {
                $this->vendor->vendor_name = trim($this->vendor->vendor_name);
                $vendor = Vendor::where('vendor_name', '=', $this->vendor->vendor_name)->where('country', '=', 'US')->first();

                if(empty($vendor->website) || empty($vendor->notes)) {
                    $vendor->website = trim($this->vendor->website);
                    $vendor->notes = trim($this->vendor->notes);
                    $vendor->save();
                }
            }

        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }
}
