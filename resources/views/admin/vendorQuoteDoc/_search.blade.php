<div class="page-header col-sm-12" style="margin-top:19px;">
    <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
        <span class="pull-right" style="margin-top: -3px;">
            <a class="btn btn-success btn-sm" href="{{route('admin.vendor-quote-doc.create', ['quote_id'=>$quoteId, 'vendor_id'=>$vendorId])}}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                Add Quote Document
            </a>
            <a class="btn btn-warning btn-sm" href="{{route('admin.vendor-quote.index', ['vendor_id'=>$vendorId])}}">
                <i class="fa fa-usd" aria-hidden="true"></i>
                Back to Quote List
            </a>
        </span>
    </div>
</div>
