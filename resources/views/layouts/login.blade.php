<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ trans('templates.system_title') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/ace/assets/css/jquery-ui-1.10.3.full.min.css">
        <link rel="stylesheet" href="/ace/assets/css/datepicker.css">
        <link rel="stylesheet" href="/ace/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="/ace/assets/css/ace.min.css">
        <link rel="stylesheet" href="/ace/assets/css/ace-rtl.min.css">
        <link rel="stylesheet" href="/ace/assets/css/ace-skins.min.css">
        <!-- basic styles -->
        <link rel="stylesheet" href="{{ elixir('css/all.css') }}">

        <!-- fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300" />
    </head>

    <body class="login-layout">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        @yield('content')
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div><!-- /.main-container -->
    <script src="{{ elixir('js/all.js') }}"></script>
</body>
</html>
