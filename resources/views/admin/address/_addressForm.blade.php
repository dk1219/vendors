{{-- vendor address region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if (Route::currentRouteName() === 'admin.vendor-address.create')
    <h3 class="header smaller lighter green">Create Vendor Address</h3>
    {!! Form::open(['route' => 'admin.vendor-address.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_vendor_address_form'])
    !!}
@else
    <h3 class="header smaller lighter green">Edit Vendor Address</h3>
    {!! Form::Model($vendorAddress, ['route' => ['admin.vendor-address.update', $vendorAddress->address_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_vendor_address_form'])
     !!}
@endif


<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Address:
    </label>
    <div class="col-sm-8">
        {!! Form::text("address1", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
    </label>
    <div class="col-sm-8">
        {!! Form::text("address2", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        City:
    </label>
    <div class="col-sm-3">
        {!! Form::text("city", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        State:
    </label>
    <div class="col-sm-3">
        {!! Form::text("state", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Zip:
    </label>
    <div class="col-sm-3">
        {!! Form::text("zip", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Country:
    </label>
    <div class="col-sm-3">
        {!! Form::text("country", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        First Name:
    </label>
    <div class="col-sm-3">
        {!! Form::text("first_name", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Last Name:
    </label>
    <div class="col-sm-3">
        {!! Form::text("last_name", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Phone:
    </label>
    <div class="col-sm-3">
        {!! Form::text("phone", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Email:
    </label>
    <div class="col-sm-3">
        {!! Form::text("email", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Ship Address:
    </label>
    <div class="col-sm-3">
        {!! Form::select('shipfrom_flag', [1=>'YES', 0=>'NO'], null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Bill Address:
    </label>
    <div class="col-sm-3">
        {!! Form::select('billto_flag', [1=>'YES', 0=>'NO'], null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Default Address:
    </label>
    <div class="col-sm-9">
        {!! Form::select('is_default', [0=>'NO', 1=>'YES'], null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>


<div class="space-20"></div>
<input type="hidden" name="vendor_id" id="vendor_id" value="{{$vendorId}}">

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-info" id="vendor_doc_submit_btn" type="submit">
            @if (Route::currentRouteName() === 'admin.vendor-address.create')
                Save New Vendor Address
            @else
                Update Vendor Address
            @endif
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- vendor address region end --}}
