<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class VendorIngredientDocRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = $this->route()->getName();

        if($routeName == 'admin.vendor-ingredient-doc.update'){
            $rules = [
                'doc' => "max:10000"
            ];
        } else {
            $rules = [
                'doc' => "required|max:10000"
            ];
        }

        return $rules;
    }
}
