<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', ['uses' => 'Admin\IndexController@index', 'as' => 'home', 'middleware' => 'auth']);

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
    Route::controller('login',
        'LoginController',
        [
            'getIndex' => 'admin.login.index',
            'postSignIn' => 'admin.login.sign_in',
            'getSignOut' => 'admin.login.sign_out',
        ]
    );

    Route::group(['middleware' => 'auth'], function() {
        Route::get('/', ['uses' => 'IndexController@index', 'as' => 'admin.home']);
        Route::get('user/list', ['uses' => 'UserController@userList', 'as' => 'admin.user-list']);
        Route::get('user/profile', ['uses' => 'UserController@profile', 'as' => 'admin.user.profile']);
        Route::put('user/update-profile', ['uses' => 'UserController@updateProfile', 'as' => 'admin.user.update.profile']);
        Route::get('ingredient/related/{id}', ['uses' => 'IngredientController@related', 'as' => 'admin.ingredient.related']);

        Route::get('vendor-ingredient-doc/download/{docId}', ['uses' => 'VendorIngredientDocController@download', 'as' => 'admin.vendor_ingredient_doc.download']);
        Route::get('vendor-ingredient-doc/doc-type', ['uses' => 'VendorIngredientDocController@getDocTypes', 'as' => 'admin.vendor_ingredient_doc.doc_type']);
        Route::get('vendor/check-docs/{vendorId}/{ingredientId}', ['uses' => 'VendorController@checkDocs', 'as' => 'admin.vendor.check_docs']);
        Route::get('vendor-quote-doc/download/{docId}', ['uses' => 'VendorQuoteDocController@download', 'as' => 'admin.vendor_quote_doc.download']);
        Route::resource('ingredient', 'IngredientController');
        Route::resource('vendor', 'VendorController');
        Route::resource('vendor-ingredient-doc', 'VendorIngredientDocController');
        Route::resource('vendor-address', 'VendorAddressController');
        Route::resource('vendor-quote', 'VendorQuoteController');
        Route::resource('vendor-quote-doc', 'VendorQuoteDocController');
        Route::resource('vendor-contact', 'ContactController');
    });

    Route::group(['middleware' => 'mustBeAdmin'], function(){
        Route::resource('user', 'UserController');

        //for tools
        Route::get('tools/import-vendors', ['uses' => 'ToolsController@importVendors', 'as' => 'admin.tools.import_vendors']);
    });
});
