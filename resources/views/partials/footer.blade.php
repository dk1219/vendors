<footer>
    <div class="footer-container">
        <div class="container">
            <div class="footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-1"> &nbsp;</div>
                    <div class="col-xs-12 col-sm-4">
                        <address class="copyright">© 2015 Green Wave Ingredients Inc.. All Rights Reserved.</address>
                    </div>
                    <div class="col-xs-12 col-sm-7"><p style="color:#aaa;margin-top:6px; font-size: 13px;">Ingredientsonline.com is an online service platform exclusively managed by Green Wave Ingredients (GWI).</p></div>
                </div>
            </div>
        </div>
    </div>
</footer>