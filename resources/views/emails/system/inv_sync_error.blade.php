@extends('layouts.email')
@section('title')
    Inventory Sync Error Notice
@stop
@section('css')

@stop
@section('content')
    Inventory sync error. <br>
    SKU: {{ $sku }} <br>
    Warehouse ID: {{ $warehouseId }} <br>
@stop