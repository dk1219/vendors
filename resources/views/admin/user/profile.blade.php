@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        <div class="widget-box transparent">
            {{-- comments region begin --}}
            <div class="alert alert-danger hidden">
                <strong>
                    <div class="validation-errors"></div>
                </strong>
            </div>

            <h3 class="header smaller lighter green">My Profile</h3>
            {!! Form::Model($user, ['route' => ['admin.user.update.profile'],
                'method' => 'PUT',
                'class' => 'form-horizontal',
                'id' => 'edit_user_form'])
             !!}

            <div class="form-group" style="margin-top: 50px;">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    User Id:
                </label>
                <div class="col-sm-9">
                    {!! Form::text('user_id', null, ['class' => 'col-xs-10 col-sm-10', 'readonly',
                        'placeholder' =>  trans('validation.required', ['attribute' => 'Ingredient Name'])
                        ])
                    !!}
                </div>
            </div>
            <div class="space-4"></div>

            <div class="form-group">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    User Full Name:
                </label>
                <div class="col-sm-9">
                    {!! Form::text('fullname', null, ['class' => 'col-xs-10 col-sm-10']) !!}
                </div>
            </div>
            <div class="space-4"></div>

            <div class="form-group">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    Email:
                </label>
                <div class="col-sm-9">
                    {!! Form::text('email', null, ['class' => 'col-xs-10 col-sm-10']) !!}
                </div>
            </div>
            <div class="space-4"></div>

            <div class="form-group">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    Role:
                </label>
                <div class="col-sm-9">
                    {!! Form::text('role', null, ['class' => 'col-xs-10 col-sm-10', 'readonly']) !!}
                </div>
            </div>
            <div class="space-4"></div>

            <hr>

            <div class="form-group">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    Reset Password:
                </label>
                <div class="col-sm-9">
                    {!! Form::checkbox('reset_password', 'Y', false, ['style' => 'margin-top:9px;', 'id'=>'reset_pass']) !!}
                </div>
            </div>
            <div class="password_area" style="display: none;">
                <div class="space-4"></div>
                <div class="form-group">
                    <label for="form-field-1" class="col-sm-2 control-label ">
                        New Password:
                    </label>
                    <div class="col-sm-9">
                        {!! Form::password('new_password', ['class' => 'col-xs-10 col-sm-10']) !!}
                    </div>
                </div>
                <div class="space-4"></div>

                <div class="form-group">
                    <label for="form-field-1" class="col-sm-2 control-label ">
                        Retype Password:
                    </label>
                    <div class="col-sm-9">
                        {!! Form::password('new_password_confirmation', ['class' => 'col-xs-10 col-sm-10']) !!}
                    </div>
                </div>
            </div>
            <div class="space-20"></div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn btn-info" id="user_update_btn" type="submit">
                        Update User
                        <i class="icon-ok bigger-110"></i>
                    </button>
                    <button class="btn btn-warning btn-back" type="button">
                        {{ trans('templates.global.back') }}
                        <i class="icon-undo bigger-110"></i>
                    </button>
                </div>
            </div>

            {!! Form::close() !!}
            {{-- lead region end --}}
        </div>
    </div>
@stop