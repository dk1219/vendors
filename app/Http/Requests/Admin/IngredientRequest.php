<?php

namespace App\Http\Requests\Admin;

use App\Consts\TbName;
use App\Http\Requests\Request;

class IngredientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = $this->route()->getName();
        $ingredientId = Request::segment(3);

        $rules = [
            'ingredient_name'=>"required|unique:".TbName::INGREDIENT_TB.",ingredient_name",
        ];

        if($routeName == 'admin.ingredient.update'){
            $rules['ingredient_name'] = "required|unique:".TbName::INGREDIENT_TB.",ingredient_name,".$ingredientId.",id";
        }

        return $rules;
    }
}
