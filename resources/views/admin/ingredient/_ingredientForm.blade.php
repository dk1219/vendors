{{-- comments region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if(Route::currentRouteName() === 'admin.ingredient.create' )
    <h3 class="header smaller lighter green">Add New Ingredient</h3>
    {!! Form::open(['route' => 'admin.ingredient.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_ingredient_form'])
    !!}
@else
    <h3 class="header smaller lighter green">Edit Ingredient</h3>
    {!! Form::Model($ingredient, ['route' => ['admin.ingredient.update', $ingredient->ingredient_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_ingredient_form'])
     !!}
@endif

<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Ingredient Name:
    </label>
    <div class="col-sm-9">
        {!! Form::text('ingredient_name', null, ['class' => 'col-xs-10 col-sm-10',
            'placeholder' =>  trans('validation.required', ['attribute' => 'Ingredient Name'])
            ])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Chempax Reference:
    </label>
    <div class="col-sm-9">
        {!! Form::text('chempax_ref', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

@include('admin.ingredient._aliasForm')
<div class="space-20"></div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        @if(Route::currentRouteName() === 'admin.ingredient.create' )
            <button class="btn btn-info" id="ingredient_add_btn" type="submit">
                Add New Ingredient
                <i class="icon-ok bigger-110"></i>
            </button>
        @else
            <button class="btn btn-info" id="ingredient_update_btn" type="submit">
                Update Ingredient
                <i class="icon-ok bigger-110"></i>
            </button>
        @endif
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- lead region end --}}