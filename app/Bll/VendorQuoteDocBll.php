<?php
/**
 * Vendor Qutoe Doc Business Logic Layer
 * User: kongdaniel
 */

namespace App\Bll;


use App\Models\Vendor;
use App\Models\VendorIngredientDoc;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

/**
 * Class VendorIngredientDocBll
 * @package App\Bll
 */
class VendorQuoteDocBll extends BaseBll
{

    /**
     * @param $file
     * @param $vendorId
     * @param $quoteId
     * @return bool
     */
    public static function uploadDocument($file, $vendorId, $quoteId)
    {
        if(!$file->isValid()) return false;

        $directory = $vendorId.'/'.$quoteId;

        $savePath = $directory.'/'.$file->getClientOriginalName();
        $succ = Storage::disk('s3_vq_doc')->put(
            $savePath,
            file_get_contents($file->getRealPath())
        );
        if(!$succ) return false;

        return true;
    }


    /**
     * @param $vendorId
     * @param $quoteId
     * @param $docName
     * @return mixed
     */
    public static function downloadDocument($vendorId, $quoteId, $docName)
    {
        $directory = $vendorId.'/'.$quoteId;
        $savePath = $directory.'/'.$docName;

        if(!self::docIsExistS3($savePath)) {
            abort(404);
        }
        $file = Storage::disk('s3_vq_doc')->get($savePath);
        $mimetype = Storage::disk('s3_vq_doc')->mimeType($savePath);
        return (new Response($file, 200))
            ->header('Content-Type', $mimetype)
            ->header('Content-Disposition', 'attachment; filename="'.$docName.'"');
    }

    /**
     * @param $savePath
     * @return bool
     */
    public static function docIsExistS3($savePath)
    {
        return Storage::disk('s3_vq_doc')->has($savePath) ? true : false;
    }


    /**
     * @param $vendorId
     * @param $quoteId
     * @param $docName
     */
    public static function deleteDocS3($vendorId, $quoteId, $docName)
    {
        $directory = $vendorId.'/'.$quoteId;
        $savePath = $directory.'/'.$docName;

        if(!self::docIsExistS3($savePath)) {
            abort(404);
        }

        Storage::disk('s3_vq_doc')->delete($savePath);
    }
}