@if (Session::has('warning'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
                <i class="icon-remove"></i>
        </button>
        <strong>
            {{ Session::get('warning') }}
        </strong>
    </div>
@endif

