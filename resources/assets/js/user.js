(function($){
    $(function(){
        if($('#reset_pass').prop('checked')==true) {
            $('.password_area').show('slow');
        }

        $('#reset_pass').on('change', function(){
            $('.password_area').toggle('slow');
        });
    });
})(jQuery);
