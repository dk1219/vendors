(function($) {
    $(function() {

        $('#vendor_ingredient').on('change', function() {
            var ingredient_id = this.value;
            var vendor_id = $('#vendor_id').val();
            $('body').mask('Loading...');

            if(ingredient_id.length == 0) {
                $('#vendor_ingredient_doc_type').empty();
                $('body').unmask();
                return false;
            }

            $.ajax({
                type:'GET',
                url: doc_type_url,
                data:{
                    'ingredient_id': ingredient_id,
                    'vendor_id': vendor_id
                },
                success:function(data){
                    var options = '';
                    $.each(data, function(doc_type, type_name){
                        options += '<option value="' + doc_type + '">' + type_name + '</option>';
                    });
                    $('#vendor_ingredient_doc_type').empty().append(options);
                    $('body').unmask();
                },
                error: function(xhr, textStatus, thrownError){
                    alert('Some Error! Try again please!');
                    $('body').unmask();
                }
            });
        });

    })
})(jQuery)