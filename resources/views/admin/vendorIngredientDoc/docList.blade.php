@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="doc_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        @include('admin.vendor._nav')

        <div class="header green col-sm-12">
            <h3 class="smaller lighter col-sm-9 lead-title">Vendor Ingredient Documents List</h3>
            <div class="blue col-sm-3 lead-total">Total Documents: <span>{{$vendorIngredientDocs->total()}}</span></div>
        </div>

        @include('admin.vendorIngredientDoc._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th>Ingredient Name</th>
                    <th>Doc Name</th>
                    <th>Doc Type</th>
                    <th>Expire On</th>
                    <th>{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$vendorIngredientDocs->isEmpty())
                    @foreach ($vendorIngredientDocs as $key => $doc)
                        @if($doc->ingredient->active)
                            <tr >
                                <td class="center">
                                    <span class="lbl">{{$key+1}}</span>
                                </td>
                                <td>
                                    {{ $doc->ingredient->ingredient_name }}
                                </td>
                                <td>
                                    {{ $doc->doc_name }}
                                </td>
                                <td>
                                    {{ $doc->doc_type }}
                                </td>
                                <td>
                                    {{ $doc->expire_on->toDateString() }}
                                </td>
                                <td style="width: 150px;">
                                    <div class="btn-group">
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.vendor-ingredient-doc.edit', $doc) }}">
                                            <i class="icon-edit bigger-120"></i>
                                        </a>
                                        @if(\App\Bll\VendorIngredientDocBll::docIsExistS3($doc->vendor_id.'/'.$doc->ingredient_id.'/'.$doc->doc_type.'/'.$doc->doc_name))
                                            <a class="btn btn-xs btn-warning" href="{{ route('admin.vendor_ingredient_doc.download', $doc) }}">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                        @endif

                                        <a class="btn btn-xs btn-danger delete-model" data-url="{{ route('admin.vendor-ingredient-doc.destroy', $doc) }}">
                                            <i class="icon-trash bigger-120"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

@stop