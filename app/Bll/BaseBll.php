<?php

namespace App\Bll;
use Illuminate\Support\Facades\DB;

/**
 * Business Logic Layer base class
 */

abstract class BaseBll
{
    /**
     *  Set ANSI for view
     */
    protected static function setAnsi()
    {
        DB::select( DB::raw("SET ANSI_NULLS ON") );
        DB::select( DB::raw("SET ANSI_WARNINGS ON") );
    }
}