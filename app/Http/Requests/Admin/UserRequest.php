<?php

namespace App\Http\Requests\Admin;

use App\Consts\TbName;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = $this->route()->getName();

        if($routeName == 'admin.user.update.profile'){
            $rules = [
                'new_password' => 'confirmed|required_if:reset_password,Y',
                'email' => 'email|required|unique:sqlsrv.'.TbName::USER_TB.',email,'.Auth::User()->admin_user_id.',admin_user_id'
            ];
        }

        if($routeName == 'admin.user.store'){
            $rules = [
                'fullname' => 'required',
                'email' => 'email|required|unique:sqlsrv.'.TbName::USER_TB.',email',
                'user_id' => 'required',
                'password' => 'confirmed|required',
            ];
        }

        if($routeName == 'admin.user.update'){
            $userId = Request::segment(3);
            $rules = [
                'fullname' => 'required',
                'email' => 'email|required|unique:sqlsrv.'.TbName::USER_TB.',email,'.$userId.',admin_user_id',
                'user_id' => 'required',
                'password' => 'confirmed',
            ];
        }

        return $rules;
    }
}
