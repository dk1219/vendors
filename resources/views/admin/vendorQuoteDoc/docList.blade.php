@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="doc_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        @include('admin.vendor._nav')

        <div class="header green col-sm-12">
            <h3 class="smaller lighter col-sm-9 lead-title">Vendor Quote Documents List</h3>
            <div class="blue col-sm-3 lead-total">Total Documents: <span>{{$vendorQuoteDocs->total()}}</span></div>
        </div>

        @include('admin.vendorQuoteDoc._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th>Document Name</th>
                    <th>{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$vendorQuoteDocs->isEmpty())
                    @foreach ($vendorQuoteDocs as $key => $doc)
                        <tr >
                            <td class="center">
                                <span class="lbl">{{$key+1}}</span>
                            </td>
                            <td>
                                {{ $doc->doc_name }}
                            </td>
                            <td style="width: 150px;">
                                <div class="btn-group">
                                    @if(\App\Bll\VendorQuoteDocBll::docIsExistS3($vendorId.'/'.$doc->quote_id.'/'.$doc->doc_name))
                                        <a class="btn btn-xs btn-warning" href="{{ route('admin.vendor_quote_doc.download', $doc) }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                    @endif

                                    <a class="btn btn-xs btn-danger delete-model" data-url="{{ route('admin.vendor-quote-doc.destroy', $doc) }}">
                                        <i class="icon-trash bigger-120"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

@stop