<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

/**
 * Class VendorAddressRequest
 * @package App\Http\Requests\Admin
 */
class VendorAddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $vendorId = $this->request->get('vendor_id');

        return [
            'address1' => "required",
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'is_default' => "default_address_exist:{$vendorId}"
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'address1.required' => 'Address is required.',
            'is_default.default_address_exist' => 'The vendor has already had default address.'
        ];
    }
}
