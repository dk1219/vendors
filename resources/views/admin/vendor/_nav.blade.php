<nav class="navbar navbar-light bg-faded lead-nav">
  <a class="navbar-brand" href="{{ route('admin.vendor.index') }}">Vendor</a>
  <ul class="nav navbar-nav">
    <li class="nav-item {{ (isset($nav_name) && $nav_name =='vendor_info') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('admin.vendor.edit', $vendorId) }}">Vendor Info</a>
    </li>
  </ul>
  <ul class="nav navbar-nav">
    <li class="nav-item {{ (isset($nav_name) && $nav_name =='vendor_address') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('admin.vendor-address.index', ['vendor_id'=>$vendorId]) }}">Vendor Address</a>
    </li>
  </ul>
  <ul class="nav navbar-nav">
    <li class="nav-item {{ (isset($nav_name) && $nav_name =='vendor_contact') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('admin.vendor-contact.index', ['vendor_id'=>$vendorId]) }}">Vendor Contact</a>
    </li>
  </ul>
  <ul class="nav navbar-nav">
    <li class="nav-item {{ (isset($nav_name) && $nav_name =='vendor_ingredient_doc') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('admin.vendor-ingredient-doc.index', ['vendor_id'=>$vendorId]) }}">Ingredient's Documents</a>
    </li>
  </ul>
  <ul class="nav navbar-nav">
    <li class="nav-item {{ (isset($nav_name) && $nav_name =='vendor_quote') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('admin.vendor-quote.index', ['vendor_id'=>$vendorId]) }}">Vendor Quote</a>
    </li>
  </ul>
</nav>