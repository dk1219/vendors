(function($){
    $(function(){
        $('#add_alias_btn').on('click', function(){
            var new_alias = $('#new_alias').val();

            if(new_alias=='') {
                alert('Please enter the alias!');
                return false;
            }

            var new_alias_tr = ' <tr>' +
                '<td class="center">' + new_alias + ' <input type="hidden" name="alias_name[]" value="' + new_alias + '"> </td> ' +
                '<td class="center"><a class="btn btn-xs btn-danger del-alias"> <i class="icon-trash bigger-120"></i></a></td> ' +
                '</tr>';

            $(new_alias_tr).hide().appendTo('#new_alias_container tbody').fadeIn('slow');
            $('#new_alias').val('');

            //check if there are ingredients in table
            if($('#new_alias_container tbody').children().length==1) {
                $('#new_alias_container').show('slow');
            }
        });

        $('#new_alias_container tbody').on('click', '.del-alias',function(){
           $(this).parents('tr').fadeOut('slow', function(){
               $(this).remove();
               //check if there are ingredients in table
               if($('#new_alias_container tbody').children().length==0) {
                   $('#new_alias_container').hide('slow');
               }
           });
        });

        /**
         *   Remove Ingredients
         */
        $('.remove_alias_btn').on('click', function(){
            $(this).parent().fadeOut('slow', function(){
                $(this).remove();
            });
        });


        var oTable1 = $('#related-product-table').dataTable( {
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true }
            ] } );

        $('table th input:checkbox').on('click' , function(){
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                .each(function(){
                    this.checked = that.checked;
                    $(this).closest('tr').toggleClass('selected');
                });

        });

        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
            return 'left';
        }

    });
})(jQuery);
