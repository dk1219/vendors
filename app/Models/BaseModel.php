<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class BaseModel
 * @package App\Models
 */
class BaseModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     *  Set ANSI for view
     */
    protected function setAnsi()
    {
        DB::connection('sqlsrv')->select( DB::raw("SET ANSI_NULLS ON") );
        DB::connection('sqlsrv')->select( DB::raw("SET ANSI_WARNINGS ON") );
    }
}
