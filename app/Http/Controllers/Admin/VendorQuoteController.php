<?php

namespace App\Http\Controllers\Admin;

use App\Bll\VendorQuoteBll;
use App\Http\Controllers\BaseController;
use App\Models\Package;
use App\Models\Vendor;
use App\Models\VendorQuote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\VendorQuoteRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

/**
 * Class VendorQuoteController
 * @package App\Http\Controllers\Admin
 */
class VendorQuoteController extends BaseController
{
    /**
     * @var VendorQuote
     */
    protected $vendorQuote;

    /**
     * VendorQuoteController constructor.
     * @param $vendorQuote
     */
    public function __construct(VendorQuote $vendorQuote)
    {
        parent::__construct();
        $this->sidebar('vendors');
        $this->navName('vendor_quote');
        $this->vendorQuote = $vendorQuote;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendorId = $request->get('vendor_id');

        if($request->get('filter')) {
            $filter = $request->get('filter');
            Session::put('filter.vendor_quote', $filter);
        }elseif(Session::has('filter.vendor_quote')){
            $filter = Session::get('filter.vendor_quote');
        }else{
            $filter = null;
        }

        $vendorQuotes = VendorQuote::with('ingredient')->search($filter)->where('vendor_id', $vendorId)
            ->paginate(config('view.paginations.admin'));

        $ingredients = Vendor::findOrFail($vendorId)->ingredients()->orderBy('ingredient_name')->lists('ingredient_name', 'ingredients.ingredient_id');

        return view('admin.vendorQuote.quoteList', compact('vendorQuotes', 'vendorId', 'ingredients', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendorId = $request->get('vendor_id');
        if(empty($vendorId)) abort(404);

        $ingredientsName = VendorQuoteBll::getIngredientsName($vendorId);
        $quoteFor = VendorQuoteBll::getQuoteFor();
        $packages = VendorQuoteBll::getPackageTypes();

        return view('admin.vendorQuote.quote', compact('ingredientsName', 'vendorId', 'quoteFor', 'packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendorQuoteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorQuoteRequest $request)
    {
        $vendorId = $request->get('vendor_id');

        foreach ($request->only('ingredient_id', 'qty', 'uom', 'unit_price', 'expire_on', 'contact', 'note', 'quoted_for', 'quote_ref',
            'vendor_id', 'shipfrom', 'selling_price', 'qty_max', 'package_type') as $key => $value) {
            $this->vendorQuote->$key = trim($value);
        }
        $this->vendorQuote->created_id = Auth::User()->admin_user_id;
        $this->vendorQuote->created_date = Carbon::now();
        $this->vendorQuote->updated_by = Auth::User()->admin_user_id;
        $this->vendorQuote->updated_date = Carbon::now();

        $result = $this->vendorQuote->save();

        if($result) {
            return redirect()->route('admin.vendor-quote.index', ['vendor_id'=>$vendorId])
                ->with('success',
                    trans('notifications.crud.success.create',
                        [
                            'model' => 'Vendor Quote'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-quote.create', ['vendor_id'=>$vendorId])
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor Quote'
                        ])
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $quoteId
     * @return \Illuminate\Http\Response
     */
    public function edit($quoteId)
    {
        $vendorQuote = $this->vendorQuote->findOrFail($quoteId);
        $vendorId = $vendorQuote->vendor_id;
        $ingredientsName = VendorQuoteBll::getIngredientsName($vendorId);
        $quoteFor = VendorQuoteBll::getQuoteFor();
        $packages = VendorQuoteBll::getPackageTypes();

        return view('admin.vendorQuote.quote', compact('vendorId', 'ingredientsName', 'vendorQuote', 'quoteFor', 'packages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendorQuoteRequest  $request
     * @param  int  $quoteId
     * @return \Illuminate\Http\Response
     */
    public function update(VendorQuoteRequest $request, $quoteId)
    {
        $vendorQuote = $this->vendorQuote->findOrFail($quoteId);

        foreach ($request->only('ingredient_id', 'qty', 'uom', 'unit_price', 'expire_on', 'contact', 'note', 'quoted_for',
            'quote_ref', 'vendor_id', 'shipfrom', 'selling_price', 'qty_max', 'package_type') as $key => $value) {
            $vendorQuote->$key = trim($value);
        }
        $vendorQuote->updated_by = Auth::User()->admin_user_id;
        $vendorQuote->updated_date = Carbon::now();

        $result = $vendorQuote->save();

        if($result) {
            return redirect()->route('admin.vendor-quote.index', ['vendor_id'=>$vendorQuote->vendor_id])
                ->with('success',
                    trans('notifications.crud.success.update',
                        [
                            'model' => 'Vendor Quote'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-quote.edit', $quoteId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Vendor Quote'
                        ])
                );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $quoteId
     * @return \Illuminate\Http\Response
     */
    public function destroy($quoteId)
    {
        DB::beginTransaction();
        try{
            $vendorQuote = $this->vendorQuote->findOrFail($quoteId);
            $vendorId = $vendorQuote->vendor_id;

            VendorQuoteBll::deleteDocFolderS3($vendorId, $quoteId);
            $this->vendorQuote->findOrFail($quoteId)->delete();
        }catch(\Exception $e){
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }

}
