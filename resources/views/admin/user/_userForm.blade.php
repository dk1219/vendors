{{-- comments region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if(Route::currentRouteName() === 'admin.user.create' )
    <h3 class="header smaller lighter green">Add New Sales</h3>
    {!! Form::open(['route' => 'admin.user.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_user_form'])
    !!}
@else
    <h3 class="header smaller lighter green">Edit Sales</h3>
    {!! Form::Model($user, ['route' => ['admin.user.update', $user->admin_user_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_user_form'])
     !!}
@endif

<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        User's Full Name:
    </label>
    <div class="col-sm-9">
        {!! Form::text('fullname', null, ['class' => 'col-xs-10 col-sm-10',
            'placeholder' =>  trans('validation.required', ['attribute' => 'Full Name'])
            ])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        User's Email:
    </label>
    <div class="col-sm-9">
        {!! Form::text('email', null, ['class' => 'col-xs-10 col-sm-10',
            'placeholder' =>  trans('validation.required', ['attribute' => 'Salesrep\'s Email'])
            ])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        User ID:
    </label>
    <div class="col-sm-9">
        {!! Form::text('user_id', null, ['class' => 'col-xs-10 col-sm-10',
            'placeholder' =>  trans('validation.required', ['attribute' => 'User ID'])
            ])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Role:
    </label>
    <div class="col-sm-3">
        {!! Form::select('role', $roleMenu, null, ['class'=>'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Access Vendors DB:
    </label>
    <div class="col-sm-3">
        {!! Form::select('access_vendorsdb', ['0'=>'NO', '1'=>'YES'], null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<hr>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Password:
    </label>
    <div class="col-sm-9">
        {!! Form::password('password', ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Retype Password:
    </label>
    <div class="col-sm-9">
        {!! Form::password('password_confirmation', ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-20"></div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        @if(Route::currentRouteName() === 'admin.user.create' )
            <button class="btn btn-info" id="user_add_btn" type="submit">
                Add New User
                <i class="icon-ok bigger-110"></i>
            </button>
        @else
            <button class="btn btn-info" id="user_update_btn" type="submit">
                Update User
                <i class="icon-ok bigger-110"></i>
            </button>
        @endif
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- lead region end --}}
