(function($) {
    /**
     * 对于POST、PUT、DELETE请求，服务器会进行csrf验证，默认为请求添加_token参数。
     */
    var defaultAjaxMethod = $.ajax;
    $.ajax = function(options) {
        var token,
            type = options.type.toUpperCase();

        if (_.isUndefined(options.data) || _.isNull(options.data)) {
            options.data = {};
        }

        if (_.indexOf(['POST', 'PUT', 'DELETE'], type) > -1) {
            token = $("meta[name='csrf_token']").attr('content');
            if (!_.isUndefined(token)) {
                options.data._token = token;
            }
        }

        return defaultAjaxMethod(options);
    };

    /**
     *  for delete model elements
     */
    $('.delete-model').click(function() {
        var del_confirm = $('div.del_confirm').html();
        var del_success = $('div.del_success').html();
        var del_failed = $('div.del_failed').html();

        if (confirm(del_confirm)) {
            $.ajax({
                type: 'DELETE',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(del_success);
                location.reload();
            }).fail(function() {
                alert(del_failed);
            });
        }
    });

    /**
     *  for delete response model elements
     */
    $('.delete-response-model').click(function() {
        var del_confirm = $('div.del_confirm').html();
        var del_success = $('div.del_success').html();
        var del_failed = $('div.del_failed').html();

        if (confirm(del_confirm)) {
            $.ajax({
                type: 'DELETE',
                url: $(this).attr('data-url')
            }).done(function(data) {
                if(data){
                    alert(data);
                }else{
                    alert(del_success);
                    location.reload();
                }
            }).fail(function() {
                alert(del_failed);
            });
        }
    });

    /**
     *  for delete model elements
     */
    $('.delete-model-get').click(function() {
        var del_confirm = $('div.del_confirm').html();
        var del_success = $('div.del_success').html();
        var del_failed = $('div.del_failed').html();

        if (confirm(del_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(del_success);
                location.reload();
            }).fail(function() {
                alert(del_failed);
            });
        }
    });

    /**
     *  for run task model elements
     */
    $('.run-task-model').click(function() {
        var run_task_confirm = $('div.run_task_confirm').html();
        var run_task_success = $('div.run_task_success').html();
        var run_task_failed = $('div.run_task_failed').html();

        if (confirm(run_task_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(run_task_success);
                location.reload();
            }).fail(function() {
                alert(run_task_failed);
            });
        }
    });

    /**
     *  for active model elements
     */
    $('.active-model').click(function() {
        var active_confirm = $('div.active_confirm').html();
        var active_success = $('div.active_success').html();
        var active_failed = $('div.active_failed').html();

        if (confirm(active_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(active_success);
                location.reload();
            }).fail(function() {
                alert(active_failed);
            });
        }
    });


    /**
     *  for set uploaded model elements
     */
    $('.set-uploaded-model').click(function() {
        var set_uploaded_confirm = $('div.set_uploaded_confirm').html();
        var set_uploaded_success = $('div.set_uploaded_success').html();
        var set_uploaded_failed = $('div.set_uploaded_failed').html();

        if (confirm(set_uploaded_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(set_uploaded_success);
                location.reload();
            }).fail(function() {
                alert(set_uploaded_failed);
            });
        }
    });

    /**
     *  for cancel uploaded model elements
     */
    $('.cancel-uploaded-model').click(function() {
        var cancel_uploaded_confirm = $('div.cancel_uploaded_confirm').html();
        var cancel_uploaded_success = $('div.cancel_uploaded_success').html();
        var cancel_uploaded_failed = $('div.cancel_uploaded_failed').html();

        if (confirm(cancel_uploaded_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(cancel_uploaded_success);
                location.reload();
            }).fail(function() {
                alert(cancel_uploaded_failed);
            });
        }
    });

    /**
     *  for lock model elements
     */
    $('.lock-model').click(function() {
        var lock_confirm = $('div.lock_confirm').html();
        var lock_success = $('div.lock_success').html();
        var lock_failed = $('div.lock_failed').html();

        if (confirm(lock_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(lock_success);
                location.reload();
            }).fail(function() {
                alert(lock_failed);
            });
        }
    });

    /**
     *  for unlock model elements
     */
    $('.unlock-model').click(function() {
        var unlock_confirm = $('div.unlock_confirm').html();
        var unlock_success = $('div.unlock_success').html();
        var unlock_failed = $('div.unlock_failed').html();

        if (confirm(unlock_confirm)) {
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                alert(unlock_success);
                location.reload();
            }).fail(function() {
                alert(unlock_failed);
            });
        }
    });

    /**
     *  for push model elements
     */
    $('.push-model-get').click(function() {
        var push_confirm = $('div.push_confirm').html();
        var push_success = $('div.push_success').html();
        var push_failed = $('div.push_failed').html();

        if (confirm(push_confirm)) {
            $('body').mask('Loading...');
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url')
            }).done(function() {
                $('body').unmask();
                alert(push_success);
                //location.reload();
            }).fail(function() {
                $('body').unmask();
                alert(push_failed);
            });
        }
    });

    /**
     *  for back prev page button
     */
    $('.btn-back').click(function(){
        window.history.back(-1);
    });

    /**
     *  for date picker elements
     */
    $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
    $('.date-range-picker').daterangepicker().prev().on(ace.click_event, function(){
        $(this).next().focus();
    });

    /**
     *  for popover
     */
    $('[data-rel=popover]').popover({html:true});

    /**
     *  for chosen
     */
    $('.chosen').chosen();

})(jQuery);