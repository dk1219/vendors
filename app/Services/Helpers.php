<?php
/**
 *  转换日期格式 12/09/2016 至 2016-12-09
 */
if(!function_exists('trans_date_format')) {
    function trans_date_format($date, $revert=false) {
        if($revert) {
            //匹配日期格式
            if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts)) {
                $date = $parts[2].'/'.$parts[3].'/'.$parts[1];
            }
        }else{
            //匹配日期格式
            if (preg_match ("/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/", $date, $parts)) {
                $date = $parts[3].'-'.$parts[1].'-'.$parts[2];
            }
        }

        return $date;
    }
}

/**
 *  Check if is admin
 */
if(!function_exists('is_admin')) {
    function is_admin($role) {
        $role = strtolower( trim($role) );

        if($role=='admin') {
            return true;
        }

        return false;
    }
}