<?php

namespace App\Models;

use App\Consts\TbName;

class Package extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::PACKAGE_VIEW;


    /**
     * @var string
     */
    public $primaryKey = 'Packaging-code';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Packaging-code', 'Description'
    ];
}
