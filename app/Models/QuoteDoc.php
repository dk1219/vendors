<?php

namespace App\Models;

use App\Consts\TbName;

/**
 * Class QuoteDoc
 * @package App\Models
 */
class QuoteDoc extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::QUOTE_DOCS;

    /**
     * @var string
     */
    public $primaryKey = 'quote_doc_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote_id', 'doc_name', 'file_path',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function VendorQuote()
    {
        return $this->belongsTo(VendorQuote::class, 'quote_id', 'quote_id');
    }
}
