(function($){
    $(function(){

        //Add new ingredient for vendor
        $('#add_ingredient_btn').on('click', function(){
            var new_ingredient_id = $('#new_ingredient').val();
            var new_ingredient_name = $('#new_ingredient').find("option:selected").text();

            if(new_ingredient_id=='') {
                alert('Please chonse the ingredient!');
                return false;
            }

            var new_ingredient = ' <tr>' +
                '<td>' + new_ingredient_name + ' <input type="hidden" name="ingredient_id[]" value="' + new_ingredient_id + '"> </td> ' +
                '<td><a class="btn btn-xs btn-danger del-ingredient"> <i class="icon-trash bigger-120"></i></a></td> ' +
                '</tr>';

            $(new_ingredient).hide().appendTo('#new_ingredients_container tbody').fadeIn('slow');

            //check if there are ingredients in table
            if($('#new_ingredients_container tbody').children().length==1) {
                $('#new_ingredients_container').show('slow');
            }
        });

        //click del new ingredient
        $('#new_ingredients_container tbody').on('click', '.del-ingredient',function(){
            $(this).parents('tr').fadeOut('slow', function(){
                $(this).remove();
                //check if there are ingredients in table
                if($('#new_ingredients_container tbody').children().length==0) {
                    $('#new_ingredients_container').hide('slow');
                }
            });
        });

        /**
         *   Remove Ingredients
         */
        $('.remove_ingredient_btn').on('click', function(){
            $.ajax({
                type:'GET',
                url: $(this).attr('remove-url'),
                success:function(data){
                    if(data.no_doc) {
                        $(this).parent().fadeOut('slow', function(){
                            $(this).remove();
                        });
                    }else{
                        alert("Please remove Ingredient's documents first!");
                    }
                }.bind(this),
                error: function(xhr, textStatus, thrownError){
                   alert('Some Error! Please Try Again!');
                }
            });
        });

        //more filter control
        $('#filter_btn').on('click', function(){
            var filter_btn = $(this);
            $('#filter_container').slideToggle("slow", function(){
                if($(this).is(':hidden')) {
                    $(this).children("input").each(function(){
                       $(this).val('');
                    });

                    $(this).children("select").each(function(){
                        $(this).prop('selectedIndex', 0);
                    });

                    $('#filter_text').text('More Filter');
                    filter_btn.addClass('btn-purple');
                    filter_btn.removeClass('btn-danger');
                }else{
                    $('#filter_text').text('Close Filter');
                    filter_btn.addClass('btn-danger');
                    filter_btn.removeClass('btn-purple');
                }
            });
        });

    });
})(jQuery);
