@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
                <i class="icon-remove"></i>
        </button>
        @foreach ($errors->all() as $error)
                <strong>
                    {{ $error }}
                </strong>
        @endforeach
    </div>
@endif

