<?php
/**
 * Dict Business Logic Layer
 * User: kongdaniel
 * Date: 12/19/15
 * Time: 6:41 PM
 */

namespace App\Bll;


use App\Consts\VendorType;
use App\Models\Dict;

class DictBll extends BaseBll
{
    /**
     *  Get Vendor Types
     *
     */
    public static function getVendorTypes()
    {
        $vendorTypes = [
            VendorType::FACTORY => 'Factory',
            VendorType::DISTRIBUTOR => 'Distributor',
            VendorType::BROKER => 'Broker',
            VendorType::COMPETITOR => 'Competitor',
            VendorType::FACTORY_DISTRIBUTOR => 'Factory / Distributor',
            VendorType::FACTORY_BROKER => 'Factory / Broker',
            VendorType::IMPORTER => 'Importer',
            VendorType::IMPORTER_BROKER => 'Importer / Broker',
            VendorType::TBD => 'TBD',
        ];

        return $vendorTypes;
    }

    /**
     * Get Role Menu
     *
     * @return mixed
     */
    public static function getRoleMenu()
    {
        $roleMenu = Dict::where('list_name', 'role_menu')->pluck('list_array');
        return $roleMenu;
    }

    /**
     * Get Qualifier Type
     *
     * @return mixed
     */
    public static function getQualifierType()
    {
        $qulifierType = Dict::where('list_name', 'qualifier_type_menu')->pluck('list_array');
        return $qulifierType;
    }
}