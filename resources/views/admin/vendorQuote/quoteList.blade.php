@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="doc_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        @include('admin.vendor._nav')

        <div class="header green col-sm-12">
            <h3 class="smaller lighter col-sm-9 lead-title">Vendor Quote List</h3>
            <div class="blue col-sm-3 lead-total">Total Quotes: <span>{{$vendorQuotes->total()}}</span></div>
        </div>

        @include('admin.vendorQuote._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th>Ingredient Name</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Unit Price</th>
                    <th>Expire On</th>
                    <th>{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$vendorQuotes->isEmpty())
                    @foreach ($vendorQuotes as $key => $quote)
                        @if($quote->ingredient->active)
                            <tr >
                                <td class="center">
                                    <span class="lbl">{{$key+1}}</span>
                                </td>
                                <td>
                                    {{ $quote->ingredient->ingredient_name }}
                                </td>
                                <td>
                                    {{ $quote->qty }}@if(!empty($quote->qty_max)) - {{ $quote->qty_max }} @endif
                                </td>
                                <td>
                                    {{ $quote->uom }}
                                </td>
                                <td>
                                    {{ $quote->unit_price }}
                                </td>
                                <td>
                                    {{ $quote->expire_on->toDateString() }}
                                </td>
                                <td style="width: 150px;">
                                    <div class="btn-group">
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.vendor-quote.edit', $quote) }}">
                                            <i class="icon-edit bigger-120"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger delete-model" data-url="{{ route('admin.vendor-quote.destroy', $quote) }}">
                                            <i class="icon-trash bigger-120"></i>
                                        </a>
                                        <a class="btn btn-xs btn-success" href="{{ route('admin.vendor-quote-doc.index', ['quote_id'=>$quote]) }}">
                                            <i class="fa fa-file-text-o bigger-120"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="row" style="margin-top: 30px;">
                <div class="col-sm-2 text-center">
                </div>
                <div class="col-sm-10">
                    <div class="dataTables_paginate paging_bootstrap">
                        {!! $vendorQuotes->appends(['filter'=>$filter])->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

@stop