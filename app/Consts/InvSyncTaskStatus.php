<?php
namespace App\Consts;


class InvSyncTaskStatus extends Consts
{
    const PENDING = 1;
    const FINISHED = 2;
    const ERROR = 3;
}