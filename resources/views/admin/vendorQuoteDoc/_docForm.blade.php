{{-- vendor quote doc region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if (Route::currentRouteName() === 'admin.vendor-quote-doc.create')
    <h3 class="header smaller lighter green">Create Vendor Quote Document</h3>
    {!! Form::open(['route' => 'admin.vendor-quote-doc.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'enctype'=>'multipart/form-data',
        'id' => 'create_quote_doc_form'])
    !!}
@else
    <h3 class="header smaller lighter green">Edit Vendor Ingredient Document</h3>
    {!! Form::Model($vendorQuoteDoc, ['route' => ['admin.vendor-quote-doc.update', $vendorQuoteDoc->quote_doc_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'enctype'=>'multipart/form-data',
        'id' => 'edit_quote_doc_form'])
     !!}
@endif

<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Quote Document:
    </label>
    <div class="col-sm-9">
        {!! Form::file('doc',['multiple'])!!}
    </div>
</div>
<div class="space-20"></div>

<input type="hidden" name="quote_id" id="quote_id" value="{{$quoteId}}">
<input type="hidden" name="vendor_id" id="vendor_id" value="{{$vendorId}}">

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-info" id="vendor_doc_submit_btn" type="submit">
            @if (Route::currentRouteName() === 'admin.vendor-quote-doc.create')
                Upload New Vendor Quote Document
            @else
                Update Vendor Quote Document
            @endif
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- vendor quote doc end --}}
