<?php

namespace App\Http\Controllers\Admin;

use App\Bll\DictBll;
use App\Bll\IngredientBll;
use App\Bll\VendorBll;
use App\Consts\TbName;
use App\Models\Ingredient;
use App\Models\Vendor;
use App\Http\Requests\Admin\VendorRequest;
use App\Models\VendorIngredientDoc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JavaScript;

/**
 * Class VendorController
 * @package App\Http\Controllers\Admin
 */
class VendorController extends BaseController
{
    /**
     * @var Vendor
     */
    protected $vendor;

    /**
     * LeadController constructor.
     */
    public function __construct(Vendor $vendor)
    {
        parent::__construct();
        $this->vendor = $vendor;
        $this->sidebar('vendors');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('filter')) {
            $filter = $request->get('filter');
            Session::put('filter.vendor', $filter);
        }elseif(Session::has('filter.vendor')){
            $filter = Session::get('filter.vendor');
        }else{
            $filter = null;
        }
        $sortBy = $request->get('sort_by');
        if(empty($sortBy)) $sortBy = 'vendor_name-asc';

        $vendors = $this->vendor->where('active', 1)->search($filter)->sort($sortBy)
            ->with(['Ingredients'=>function($query){
                $query->wherePivot('active', 1)
                    ->where('ingredients.active', 1);
            }])
            ->with(['Addresses'=>function($query){
                $query->where('is_default', 1)->where('active', 1);
            }])
            ->paginate(config('view.paginations.admin'));

        $isMoreFilter = VendorBll::isMoreFilter($filter);

        return view('admin.vendor.vendorList', compact('vendors', 'filter', 'sortBy', 'isMoreFilter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendorTypes = DictBll::getVendorTypes();
        $allIngredients = IngredientBll::ingredientsList();

        return view('admin.vendor.vendor', compact('vendorTypes', 'allIngredients'));
    }

    /**
     * Store a newly created resource in storage.
     * @param VendorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VendorRequest $request)
    {
        DB::beginTransaction();
        try{
            foreach ($request->only('vendor_name', 'chempax_ref', 'website', 'fd_member', 'vender_type', 'payment_term', 'notes', 'country') as $key => $value) {
                $this->vendor->$key = trim($value);
            }
            $this->vendor->created_by = Auth::User()->admin_user_id;
            $this->vendor->updated_by = Auth::User()->admin_user_id;
            $this->vendor->created_date = Carbon::now();
            $this->vendor->updated_date = Carbon::now();

            $this->vendor->save();

            $ingredientsToSync = [];
            $ingredientIds = $request->get('ingredient_id');
            if($ingredientIds) {
                foreach($ingredientIds as $key=>$ingredientId) {
                    $ingredientsToSync[$ingredientId] = [
                        'created_by' => Auth::user()->admin_user_id,
                        'created_date' => Carbon::now(),
                        'updated_by' => Auth::user()->admin_user_id,
                        'updated_date' => Carbon::now(),
                    ];
                }
            }
            $this->vendor->ingredients()->sync($ingredientsToSync);
        }catch(\Exception $e){
            DB::rollback();
            Log::error($e->getMessage());
            return redirect()->route('admin.vendor.create')
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor'
                        ])
                );
        }
        DB::commit();

        return redirect()->route('admin.vendor.index')
            ->with('success',
                trans('notifications.crud.success.create',
                    [
                        'model' => 'Vendor'
                    ])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $vendorId
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $vendorId)
    {
        if(empty($vendorId)) abort(404);
        $this->navName('vendor_info');
        $vendorTypes = DictBll::getVendorTypes();
        $allIngredients = IngredientBll::ingredientsList();

        try{
            $vendor = $this->vendor->with(['ingredients'=>function($query){
                $query->wherePivot('active', 1)
                    ->where('ingredients.active', 1);
            }])->findOrFail($vendorId);
        }catch(\Exception $e){
            Log::error($e->getMessage());
            die('Vendor Not Found!');
        }

        return view('admin.vendor.vendor', compact('vendorId', 'vendor', 'vendorTypes', 'allIngredients'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $vendorId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $vendorId)
    {
        $validator = Validator::make($request->all(), [
            "vendor_name" => "required|unique:".TbName::VENDOR_TB.",vendor_name,".$vendorId.",vendor_id,active,1",
            "chempax_ref" => 'digits:5',
            'website' => 'url'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->except('ingredient_id'));
        }

        $vendor = $this->vendor->findOrFail($vendorId);

        DB::beginTransaction();
        try{
            foreach ($request->only('vendor_name', 'chempax_ref', 'website', 'fd_member', 'vender_type', 'payment_term', 'notes', 'country') as $key => $value) {
                $vendor->$key = trim($value);
            }
            $vendor->updated_by = Auth::User()->admin_user_id;
            $vendor->updated_date = Carbon::now();
            $vendor->save();

            $ingredientsToSync = [];
            $ingredientIds = $request->get('ingredient_id');
            if($ingredientIds) {
                foreach($ingredientIds as $key=>$ingredientId) {
                    $ingredientsToSync[$ingredientId] = [
                        'created_by' => Auth::user()->admin_user_id,
                        'created_date' => Carbon::now(),
                        'updated_by' => Auth::user()->admin_user_id,
                        'updated_date' => Carbon::now(),
                    ];
                }
            }
            $vendor->ingredients()->sync($ingredientsToSync);
        }catch(\Exception $e){
            DB::rollback();
            Log::error($e->getMessage());
            return redirect()->route('admin.vendor.edit', $vendorId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Vendor'
                        ])
                );
        }
        DB::commit();

        return redirect()->route('admin.vendor.index')
            ->with('success',
                trans('notifications.crud.success.update',
                    [
                        'model' => 'Vendor'
                    ])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $vendorId
     * @return \Illuminate\Http\Response
     */
    public function destroy($vendorId)
    {
        DB::beginTransaction();
        try{
            $vendor = $this->vendor->findOrFail($vendorId);
            $vendor->active = 0;
            $vendor->save();
        }catch(\Exception $e){
            DB::rollback();
        }
        DB::commit();
    }

    /**
     * @param $vendorId
     * @param $ingredientId
     * @return mixed
     */
    public function checkDocs($vendorId, $ingredientId)
    {
        $docCounter = VendorIngredientDoc::where('vendor_id', $vendorId)->where('ingredient_id', $ingredientId)->count();

        $docCounter > 0 ? $result['no_doc']=false : $result['no_doc']=true;

        return $result;
    }

}
