@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="inv_lead_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        <div class="header green col-sm-12" style="margin-top: 0;">
            <h3 class="smaller lighter col-sm-9 lead-title">Ingredient List</h3>
            <div class="blue col-sm-3 lead-total">Total Ingredients: <span>{{$ingredients->total()}}</span></div>
        </div>

        @include('admin.ingredient._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th>Ingredient Name</th>
                    <th style="width: 100px;">Chempax Ref</th>
                    <th style="text-align: center; width: 100px;">Alias</th>
                    <th style="text-align: center;">Vendors</th>
                    <th style="width:80px;">{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$ingredients->isEmpty())
                    @foreach ($ingredients as $key => $ingredient)
                        <tr >
                            <td class="center">
                                {{ $key + 1 }}
                            </td>
                            <td>
                                {{ $ingredient->ingredient_name }}
                            </td>
                            <td>
                                {{ $ingredient->chempax_ref }}
                            </td>
                            <td style="text-align: center;">
                                @if(!$ingredient->alias->isEmpty())
                                    <span class="btn btn-success btn-xs tooltip-success" data-rel="popover" data-placement="left"
                                          data-original-title="<i class='fa fa-th-large' aria-hidden='true'></i> Alias"
                                          data-content="@foreach($ingredient->alias as $key=>$alias)
                                                  {{$key+1}} - {{$alias->alias_name}}<br /> @endforeach">
                                        Alias
                                    </span>
                                @endif
                            </td>
                            <td style="text-align: center;">
                                @if(!$ingredient->vendors->isEmpty())
                                    <span class="btn btn-success btn-xs tooltip-success" data-rel="popover" data-placement="left"
                                          data-original-title="<i class='fa fa-users' aria-hidden='true'></i> Vendors"
                                          data-content="@foreach($ingredient->vendors as $key=>$vendor)
                                                  <a href={{route('admin.vendor.edit', $vendor->vendor_id)}}>{{$key+1}} - {{$vendor->vendor_name}}</a> <br /> @endforeach">
                                        <i class='fa fa-users' aria-hidden='true'></i> Vendors
                                    </span>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.ingredient.edit', $ingredient) }}">
                                        <i class="icon-edit bigger-120"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger delete-model" data-url="{{route('admin.ingredient.destroy', $ingredient)}}">
                                        <i class="icon-trash bigger-120"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="row" style="margin-top: 30px;">
                <div class="col-sm-2 text-center">
                </div>
                <div class="col-sm-10">
                    <div class="dataTables_paginate paging_bootstrap">
                        {!! $ingredients->appends(['filter'=>$filter])->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

    </div><!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
@stop