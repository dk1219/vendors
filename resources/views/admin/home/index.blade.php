@extends('layouts.default')
@section('css')
@stop

@section('content')
    <hr>
    <div style="margin: 20px; font-size: 18px;">
        <table border="0" cellspacing="1" cellpadding="20" align="center">
            <tbody>
            <tr align="left">
                <td valign="top" height="220" width="250" align="center" bgcolor="#d9d9d9">
                    <font size="+2"><b>Vendors</b></font><br><br>
                    <a href="{{ route('admin.vendor.index') }}"><i class="fa fa-users fa-red fa-5x" style="color:green"></i></a>
                    <br><br>
                    <a href="{{ route('admin.vendor.index') }}"><i class="fa fa-search fa-1x"></i></a> Search &nbsp; &nbsp;
                    <a href="{{ route('admin.vendor.create') }}"><i class="fa fa-plus-square-o fa-1x"></i></a> Add
                </td>
                <td width="10">&nbsp;</td>


                <td valign="top" width="250" align="center" bgcolor="#d9d9d9">
                    <font size="+2"><b>Ingredients</b></font><br><br>
                    <a href="{{ route('admin.ingredient.index') }}"><i class="fa fa-flask fa-5x" style="color:#c46d00;"></i></a>
                    <br><br>
                    <a href="{{ route('admin.ingredient.index') }}"><i class="fa fa-search fa-1x"></i></a> Search &nbsp; &nbsp;
                    <a href="{{ route('admin.ingredient.create') }}"><i class="fa fa-plus-square-o fa-1x"></i></a> Add
                </td>

            </tr>
            </tbody>
        </table>
    </div>
@stop

@section('js_pre')
@stop
