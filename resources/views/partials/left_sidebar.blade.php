<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>
    <ul class="nav nav-list">
        <li class="{{ (isset($sidebar['menu']) && $sidebar['menu']=='vendors') ? 'active' : '' }}">
            <a href="#" class="dropdown-toggle" style="{{ (isset($sidebar['menu']) && $sidebar['menu']=='logs') ? 'display:block;' : '' }}">
                <i class="fa fa-users fa-1x"></i>
                <span class="menu-text"> Vendors </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="{{ route('admin.vendor.index') }}">
                        <i class="icon-double-angle-right"></i>
                        Search Vendor
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.vendor.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add Vendor
                    </a>
                </li>
            </ul>
        </li>
    </ul><!-- /.nav-list -->

    <ul class="nav nav-list">
        <li class="{{ (isset($sidebar['menu']) && $sidebar['menu']=='ingredients') ? 'active' : '' }}">
            <a href="#" class="dropdown-toggle" style="{{ (isset($sidebar['menu']) && $sidebar['menu']=='logs') ? 'display:block;' : '' }}">
                <i class="fa fa-leaf" aria-hidden="true"></i>
                <span class="menu-text"> Ingredients </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="{{ route('admin.ingredient.index') }}">
                        <i class="icon-double-angle-right"></i>
                        Search Ingredient
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.ingredient.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add Ingredient
                    </a>
                </li>
            </ul>
        </li>
    </ul><!-- /.nav-list -->

    @if(is_admin(Auth::User()->role))
        <ul class="nav nav-list">
            <li class="{{ (isset($sidebar['menu']) && $sidebar['menu']=='users') ? 'active' : '' }}">
                <a href="#" class="dropdown-toggle" style="{{ (isset($sidebar['menu']) && $sidebar['menu']=='logs') ? 'display:block;' : '' }}">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span class="menu-text"> Users </span>

                    <b class="arrow icon-angle-down"></b>
                </a>

                <ul class="submenu">
                    <li>
                        <a href="{{ route('admin.user.index') }}">
                            <i class="icon-double-angle-right"></i>
                            Search User
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.user.create') }}">
                            <i class="icon-double-angle-right"></i>
                            Add User
                        </a>
                    </li>
                </ul>
            </li>
        </ul><!-- /.nav-list -->
    @endif

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>
