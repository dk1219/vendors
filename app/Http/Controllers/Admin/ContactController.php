<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\VendorContactRequest;
use App\Http\Requests;

class ContactController extends BaseController
{
    protected $contact;

    /**
     * ContactController constructor.
     * @param $contact
     */
    public function __construct(Contact $contact)
    {
        parent::__construct();
        $this->sidebar('vendors');
        $this->navName('vendor_contact');
        $this->contact = $contact;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendorId = $request->get('vendor_id');

        if(empty($vendorId)) abort(404);

        $contacts = Contact::where('vendor_id', '=', $vendorId)->where('active', 1)->get();

        return view('admin.contact.contactList', compact('contacts', 'vendorId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendorId = $request->get('vendor_id');
        if(empty($vendorId)) abort(404);

        return view('admin.contact.contact', compact('vendorId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendorContactRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorContactRequest $request)
    {
        $vendorId = $request->get('vendor_id');

        foreach ($request->only('address1', 'address2', 'title', 'email', 'city', 'state', 'zip', 'first_name', 'last_name',
            'tel', 'fax', 'vendor_id') as $key => $value) {
            $this->contact->$key = trim($value);
        }

        $result = $this->contact->save();

        if($result) {
            return redirect()->route('admin.vendor-contact.index', ['vendor_id'=>$vendorId])
                ->with('success',
                    trans('notifications.crud.success.create',
                        [
                            'model' => 'Vendor Contact'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-contact.create', ['vendor_id'=>$vendorId])
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor Contact'
                        ])
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $contactId
     * @return \Illuminate\Http\Response
     */
    public function edit($contactId)
    {
        $vendorContact = $this->contact->findOrFail($contactId);
        $vendorId = $vendorContact->vendor_id;

        return view('admin.contact.contact', compact('vendorId', 'vendorContact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendorContactRequest $request
     * @param  int  $contactId
     * @return \Illuminate\Http\Response
     */
    public function update(VendorContactRequest $request, $contactId)
    {
        $vendorContact = $this->contact->findOrFail($contactId);
        $vendorId = $vendorContact->vendor_id;

        foreach ($request->only('address1', 'address2', 'title', 'email', 'city', 'state', 'zip', 'first_name', 'last_name',
            'tel', 'fax', 'vendor_id') as $key => $value) {
            $vendorContact->$key = trim($value);
        }
        $result = $vendorContact->save();

        if($result) {
            return redirect()->route('admin.vendor-contact.index', ['vendor_id'=>$vendorId])
                ->with('success',
                    trans('notifications.crud.success.update',
                        [
                            'model' => 'Vendor Contact'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-contact.edit', $contactId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Vendor Contact'
                        ])
                );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $contactId
     * @return \Illuminate\Http\Response
     */
    public function destroy($contactId)
    {
        $contact = $this->contact->findOrFail($contactId);
        $contact->active = 0;
        $contact->save();
    }
}
