<div class="page-header col-sm-12" style="margin-top:19px;">
    {!! Form::open(['route' => ['admin.ingredient.index'] ,'method' => 'GET', 'id' => 'ingredient_search_form']) !!}
        <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
            <input name="filter[ingredient_name]" type="text" value="{{ isset($filter['ingredient_name']) ? $filter['ingredient_name'] : ''}}" class="search-query pull-left col-xs-8"
                   placeholder="{{ trans('templates.global.search_query', ['query' => 'Ingredient Name']) }}" style="margin-right: 15px; height: 34px;">

            <span class="pull-right" style="margin-top: -3px;">
                <a class="btn btn-success btn-sm" href="{{route('admin.ingredient.create')}}">
                    <i class="fa fa-leaf" aria-hidden="true"></i>
                    Add Ingredient
                </a>
            </span>
            <span class="pull-right" style="margin-top: -3px; margin-right: 2px;">
                <button type="submit" class="btn btn-purple btn-sm">
                    <i class="icon-search icon-on-right bigger-110"></i>
                    {{ trans('templates.global.search') }}
                </button>
            </span>
        </div>

        <div class="col-xs-12" style="padding-left: 0px; margin-top: 15px;">
            <input name="filter[chempax_ref]" type="text" value="{{ isset($filter['chempax_ref']) ? $filter['chempax_ref'] : ''}}" class="search-query pull-left col-xs-5"
                   placeholder="{{ trans('templates.global.search_query', ['query' => 'Chempax Ref']) }}" style="margin-right: 15px; height: 34px;">
            <input name="filter[no_chempax]" type="checkbox" id="no_chempax" value="Y" class="search-query pull-left" @if(isset($filter['no_chempax'])) checked @endif
                   style="margin-right: 15px; margin-top: 10px;"> <label for="no_chempax" style="margin-top: 5px;">No Chempax Reference</label>
        </div>
    {!! Form::close() !!}
</div>
