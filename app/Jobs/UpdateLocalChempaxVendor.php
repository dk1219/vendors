<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Vendor;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateLocalChempaxVendor extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $vendor;

    /**
     * UpdateLocalChempaxVendor constructor.
     * @param $vendor
     */
    public function __construct($vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            if(isset($this->vendor) && isset($this->vendor->vendor_name) && !empty($this->vendor->vendor_name)) {
                $this->vendor->vendor_name = trim($this->vendor->vendor_name);
                $vendor = Vendor::where('vendor_name', '=', $this->vendor->vendor_name)->where('country', '=', 'US')->first();

                $vendor->payment_term = trim($this->vendor->payment_terms);

                $vendor->save();
            }

        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }
}
