<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Address;
use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\VendorAddressRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class VendorAddressController extends BaseController
{

    protected $address;

    /**
     * VendorAddressController constructor.
     * @param $address
     */
    public function __construct(Address $address)
    {
        parent::__construct();
        $this->sidebar('vendors');
        $this->navName('vendor_address');
        $this->address = $address;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendorId = $request->get('vendor_id');

        if(empty($vendorId)) abort(404);

        $addresses = Address::where('vendor_id', '=', $vendorId)->where('active', 1)->get();

        return view('admin.address.addressList', compact('addresses', 'vendorId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendorId = $request->get('vendor_id');
        if(empty($vendorId)) abort(404);

        return view('admin.address.address', compact('vendorId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendorAddressRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorAddressRequest $request)
    {

        $vendorId = $request->get('vendor_id');

        foreach ($request->only('address1', 'address2', 'city', 'state', 'zip', 'country', 'first_name', 'last_name',
            'phone', 'email', 'shipfrom_flag', 'billto_flag', 'is_default', 'vendor_id') as $key => $value) {
            $this->address->$key = trim($value);
        }

        $result = $this->address->save();

        if($result) {
            return redirect()->route('admin.vendor-address.index', ['vendor_id'=>$vendorId])
                ->with('success',
                    trans('notifications.crud.success.create',
                        [
                            'model' => 'Vendor Address'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-address.create', ['vendor_id'=>$vendorId])
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor Address'
                        ])
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $addressId
     * @return \Illuminate\Http\Response
     */
    public function edit($addressId)
    {
        $vendorAddress = $this->address->findOrFail($addressId);
        $vendorId = $vendorAddress->vendor_id;

        return view('admin.address.address', compact('vendorId', 'vendorAddress'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendorAddressRequest  $request
     * @param  int  $addressId
     * @return \Illuminate\Http\Response
     */
    public function update(VendorAddressRequest $request, $addressId)
    {
        $vendorAddress = $this->address->findOrFail($addressId);
        $vendorId = $vendorAddress->vendor_id;

        foreach ($request->only('address1', 'address2', 'city', 'state', 'zip', 'country', 'first_name', 'last_name',
            'phone', 'email', 'shipfrom_flag', 'billto_flag', 'is_default', 'vendor_id') as $key => $value) {
            $vendorAddress->$key = trim($value);
        }
        $result = $vendorAddress->save();

        if($result) {
            return redirect()->route('admin.vendor-address.index', ['vendor_id'=>$vendorId])
                ->with('success',
                    trans('notifications.crud.success.update',
                        [
                            'model' => 'Vendor Address'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-address.edit', $addressId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Vendor Address'
                        ])
                );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $addressId
     * @return \Illuminate\Http\Response
     */
    public function destroy($addressId)
    {
        $this->address->findOrFail($addressId)->delete();
    }
}
