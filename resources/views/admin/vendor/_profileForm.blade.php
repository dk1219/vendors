{{-- lead region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@include('admin.lead._nav')
<h3 class="header smaller lighter green">Edit Profile</h3>
{!! Form::Model($lead, ['route' => ['admin.lead.update', $lead->id],
    'method' => 'PUT',
    'class' => 'form-horizontal',
    'id' => 'edit_lead_form'])
 !!}

@include('admin.lead._headForm')

<hr />

<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Industry Type:
    </label>
    <div class="col-sm-3">
        {!! Form::select('industry_type', $industryTypes, null, ['placeholder'=>'-- Industry Type --', 'class'=>'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Headquarter / Branch:
    </label>
    <div class="col-sm-3">
        {!! Form::select('headquarters_or_branch', $headquarterBranch, null, ['placeholder'=>'-- Headquarters / Branch --', 'class'=>'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Credit Rating:
    </label>
    <div class="col-sm-3">
        {!! Form::text('credit_rating', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Credit Score:
    </label>
    <div class="col-sm-3">
        {!! Form::text('credit_score', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Primary SIC:
    </label>
    <div class="col-sm-3">
        {!! Form::text('primary_sic_code', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Primary SIC Desc:
    </label>
    <div class="col-sm-3">
        {!! Form::text('primary_sic_desc', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        2nd SIC#1:
    </label>
    <div class="col-sm-3">
        {!! Form::text('secondary_sic_code1', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        2nd SIC Desc#1:
    </label>
    <div class="col-sm-3">
        {!! Form::text('secondary_sic_desc1', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        2nd SIC#2:
    </label>
    <div class="col-sm-3">
        {!! Form::text('secondary_sic_code2', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        2nd SIC Desc#2:
    </label>
    <div class="col-sm-3">
        {!! Form::text('secondary_sic_desc2', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Public/Private:
    </label>
    <div class="col-sm-3">
        {!! Form::select('public_private_flag', $publicPrivateFlag, null, ['placeholder'=>'-- Public Private Flag --', 'class'=>'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Office Size:
    </label>
    <div class="col-sm-3">
        {!! Form::text('office_size', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Mail Route:
    </label>
    <div class="col-sm-3">
        {!! Form::text('mail_route', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Mail Bar Code:
    </label>
    <div class="col-sm-3">
        {!! Form::text('mail_barcode', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        # of Employees:
    </label>
    <div class="col-sm-3">
        {!! Form::text('num_employees', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Employee Size Range:
    </label>
    <div class="col-sm-3">
        {!! Form::text('employee_size_range', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Years Estimate:
    </label>
    <div class="col-sm-3">
        {!! Form::text('years_estimate', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Facility SQ FT:
    </label>
    <div class="col-sm-3">
        {!! Form::text('facility_sq_ft', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Franchise Specialty 1:
    </label>
    <div class="col-sm-3">
        {!! Form::text('franchise_specialty1', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Franchise Specialty 2:
    </label>
    <div class="col-sm-3">
        {!! Form::text('franchise_specialty2', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        PC Code:
    </label>
    <div class="col-sm-3">
        {!! Form::text('pc_code', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        InfoUSA ID:
    </label>
    <div class="col-sm-3">
        {!! Form::text('infousa_id', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Latitude:
    </label>
    <div class="col-sm-3">
        {!! Form::text('latitude', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Adsize in YP:
    </label>
    <div class="col-sm-3">
        {!! Form::text('adsize_in_yellow_pages', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Match Code:
    </label>
    <div class="col-sm-3">
        {!! Form::text('match_code', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Metro Area:
    </label>
    <div class="col-sm-3">
        {!! Form::text('metro_area', null, ['class' => 'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Sales Revenue:
    </label>
    <div class="col-sm-3">
        {!! Form::text('sales_revenue', null, ['class' => 'col-xs-12 col-sm-12']) !!}
    </div>
</div>
<div class="space-20"></div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <input type="hidden" name="is_profile" value="1">
        <button class="btn btn-info" id="lead_submit_btn" type="submit">
                Update Lead
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- lead region end --}}
