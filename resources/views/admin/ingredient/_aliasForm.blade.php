@if(isset($ingredient) && !$ingredient->alias->isEmpty())
    <h3 class="header smaller lighter green">
        Exists Alias
    </h3>
    <div id="exist_ingredients_container">
        @foreach($ingredient->alias as $key=>$alias)
            <div class="form-group">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    Alias {{$key+1}}:
                </label>
                <div class="col-sm-6">
                    {!! Form::text("exist_alias_name[]", $alias->alias_name, ["class" => "col-xs-10 col-sm-10", 'placeholder'=>'-- ingredients --']) !!}
                </div>
                <button class="btn btn-danger btn-xs remove_alias_btn" style="margin-top: 1px;" type="button">Remove</button>
            </div>
        @endforeach
    </div>
@endif


<h3 class="header smaller lighter green" style="margin-top: 50px;">
    Add Alias
</h3>
<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Alias:
    </label>
    <div class="col-sm-8">
        {!! Form::text("alias_name_input", null, ["id"=>"new_alias", "class" => "col-xs-8 col-sm-8", 'placeholder'=>'-- alias --']) !!}
        <button class="btn btn-info btn-xs" id="add_alias_btn" style="margin-left: 10px;" type="button">
            <i class="fa fa-plus-circle" aria-hidden="true"></i>
            Add Alias
        </button>
    </div>
</div>

<table class="table table-striped table-bordered table-hover dataTable col-xs-9" id="new_alias_container" style="margin-bottom: 50px; margin-left: 10%; width: 60%; display: none;">
    <thead>
    <tr>
        <th class="center">Alias</th>
        <th style="width: 50px;">Action</th>
    </tr>
    </thead>

    <tbody>
    </tbody>
</table>
