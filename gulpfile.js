var elixir = require('laravel-elixir');
require('laravel-elixir-sass-compass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.compass('app.scss','public/css/', {
        style: "compressed",
        sass: "./resources/assets/sass"
    });

    mix.styles([
        '../bower_components/bootstrap/dist/css/bootstrap.min.css',
        '../ace/assets/css/chosen.css',
        '../ace/assets/css/daterangepicker.css',
        '../bower_components/font-awesome/css/font-awesome.min.css',
        '../bower_components/jquery-loadmask/jquery.loadmask.css'
    ]);

    mix.scripts([
        '../ace/assets/js/jquery-2.0.3.min.js',
        '../bower_components/underscore/underscore-min.js',
        '../bower_components/bootstrap/dist/js/bootstrap.min.js',
        '../bower_components/vue/dist/vue.min.js',
        '../bower_components/vue-resource/dist/vue-resource.min.js',
        '../ace/assets/js/jquery-ui-1.10.3.full.min.js',
        '../bower_components/jquery-loadmask/jquery.loadmask.min.js',
        '../bower_components/d3/d3.min.js',
        '../ace/assets/js/ace-extra.min.js',
        '../ace/assets/js/ace.min.js',
        '../ace/assets/js/bootbox.min.js',
        '../ace/assets/js/chosen.jquery.min.js',
        '../ace/assets/js/ace-elements.min.js',
        '../ace/assets/js/date-time/bootstrap-datepicker.min.js',
        '../ace/assets/js/date-time/moment.min.js',
        '../ace/assets/js/date-time/daterangepicker.min.js',
        '../ace/assets/js/jquery.dataTables.min.js',
        '../ace/assets/js/jquery.dataTables.bootstrap.js',
        'global.js',
        'vendor.js',
        'ingredient.js',
        'user.js',
        'vendor_ingredient_doc.js'
    ]);

    mix.version(['css/all.css', 'css/app.css', 'css/login.css', 'js/all.js']);

    mix.copy('resources/assets/bower_components/jquery-loadmask/images/loading.gif', 'public/build/images');
    mix.copy('resources/assets/bower_components/font-awesome/fonts', 'public/build/fonts');
});
