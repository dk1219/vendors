<?php
/**
 * Ingredient Business Logic Layer
 * User: kongdaniel
 */

namespace App\Bll;

use App\Models\Ingredient;

class IngredientBll extends BaseBll
{
    /**
     * Get ingredients list
     * @return mixed
     */
    public static function ingredientsList()
    {
        return Ingredient::where('active', 1)
            ->orderBy('ingredient_name', 'asc')
            ->lists('ingredient_name', 'ingredient_id');
    }

}