<?php

namespace App\Models;

use App\Consts\TbName;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::USER_TB;

    /**
     * The databease connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * @var string
     */
    public $primaryKey = 'admin_user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'user_password', 'access_vendorsdb'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['user_password', 'remember_token'];

    /**
     * set auth password field name
     * @return mixed
     */
    public function getAuthPassword() {
        return $this->user_password;
    }

    /**
     * Users filter search
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeSearch($query, $filter)
    {
        if (empty($filter)) {
            return $query;
        }

        foreach($filter as $key => $value) {
            if (empty(trim($value))) {
                continue;
            }
            $value = trim($value);

            if($key=='fullname') {
                $query->where($this->getTable().'.fullname', 'like', $value.'%');
            }
        }

        return $query;
    }
}
