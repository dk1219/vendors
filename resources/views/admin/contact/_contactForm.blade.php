{{-- vendor contact region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if (Route::currentRouteName() === 'admin.vendor-contact.create')
    <h3 class="header smaller lighter green">Create Vendor Contact</h3>
    {!! Form::open(['route' => 'admin.vendor-contact.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_vendor_contact_form'])
    !!}
@else
    <h3 class="header smaller lighter green">Edit Vendor Contact</h3>
    {!! Form::Model($vendorContact, ['route' => ['admin.vendor-contact.update', $vendorContact->contact_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_vendor_contact_form'])
     !!}
@endif


<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Address:
    </label>
    <div class="col-sm-8">
        {!! Form::text("address1", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
    </label>
    <div class="col-sm-8">
        {!! Form::text("address2", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Title:
    </label>
    <div class="col-sm-3">
        {!! Form::text("title", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Email:
    </label>
    <div class="col-sm-3">
        {!! Form::text("email", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        City:
    </label>
    <div class="col-sm-8">
        {!! Form::text("city", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Zip:
    </label>
    <div class="col-sm-3">
        {!! Form::text("zip", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        State:
    </label>
    <div class="col-sm-3">
        {!! Form::text("state", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        First Name:
    </label>
    <div class="col-sm-3">
        {!! Form::text("first_name", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Last Name:
    </label>
    <div class="col-sm-3">
        {!! Form::text("last_name", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        TEL:
    </label>
    <div class="col-sm-3">
        {!! Form::text("tel", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Fax:
    </label>
    <div class="col-sm-3">
        {!! Form::text("fax", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>


<div class="space-20"></div>
<input type="hidden" name="vendor_id" id="vendor_id" value="{{$vendorId}}">

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-info" id="vendor_doc_submit_btn" type="submit">
            @if (Route::currentRouteName() === 'admin.vendor-contact.create')
                Save New Vendor Contact
            @else
                Update Vendor Countact
            @endif
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- vendor contact region end --}}
