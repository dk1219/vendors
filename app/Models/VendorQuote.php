<?php

namespace App\Models;

use App\Consts\TbName;

/**
 * Class VendorQuote
 * @package App\Models
 */
class VendorQuote extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::VENDOR_QUOTE_TB;


    /**
     * @var string
     */
    public $primaryKey = 'quote_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_id', 'created_date', 'updated_by', 'updated_date', 'ingredient_id', 'vendor_id', 'qty', 'uom', 'unit_price',
        'expire_on', 'contact', 'note', 'quoted_for', 'quote_ref', 'shipfrom', 'qty_max', 'package_type', 'selling_price'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date', 'updated_date', 'expire_on'
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Ingredient()
    {
        return $this->belongsTo(Ingredient::class, 'ingredient_id', 'ingredient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function QuoteDocs()
    {
        return $this->hasMany(QuoteDoc::class, 'quote_id', 'quote_id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getUnitPriceAttribute($value)
    {
        if($value !== null) {
            return number_format($value, 2);
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getSellingPriceAttribute($value)
    {
        if($value !== null) {
            return number_format($value, 2);
        }
    }

    /**
     * Vendor Quote filter search
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeSearch($query, $filter)
    {
        if (empty($filter)) {
            return $query;
        }

        foreach($filter as $key => $value) {
            if (empty(trim($value))) {
                continue;
            }
            $value = trim($value);

            if($key=='ingredient_id') {
                $query->where(function($query) use($value){
                    $query->where($this->getTable().'.ingredient_id', '=', $value);
                });
            }
        }

        return $query;
    }
}
