@extends('layouts.login')
@section('content')
    <div class="center">
        <h1>
            <i class="icon-leaf green"></i>
            <span class="red">{{ trans('templates.system_title') }}</span>
            <span class="white">{{ trans('templates.login.admin_title') }}</span>
        </h1>
        <h4 class="blue">&copy; GWI</h4>
    </div>

    <div class="space-6"></div>

    <div class="position-relative">
        <div id="login-box" class="login-box visible widget-box no-border">
            <div class="widget-body">
                <div class="widget-main">
                    <h4 class="header blue lighter bigger">
                        <i class="icon-coffee green"></i>
                        {{ trans('templates.login.fill_tips') }}
                    </h4>
                    <div class="space-6"></div>
                    {!! Form::open(['route' => 'admin.login.sign_in', 'id' => 'contact']) !!}
                    <fieldset>
                        @include('partials.errors')
                        <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                            <input type="text" name="email" class="form-control" placeholder="email" />
                            <i class="icon-user"></i>
                        </span>
                        </label>
                        <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                            <input type="password" name="password" class="form-control" placeholder="Password" />
                            <i class="icon-lock"></i>
                        </span>
                        </label>
                        <div class="space"></div>
                        <div class="clearfix">
                            <label class="inline">
                                <input type="checkbox" name="remember" class="ace" />
                                <span class="lbl"> {{ trans('templates.login.remember_me') }}</span>
                            </label>
                            <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                <i class="icon-key"></i>
                                Login
                            </button>
                        </div>
                        <div class="space-4"></div>
                    </fieldset>
                    {!! Form::close() !!}
                </div><!-- /widget-main -->
            </div><!-- /widget-body -->
        </div><!-- /login-box -->
    </div>
@stop