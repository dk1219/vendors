<?php
namespace App\Services;

use App\Models\Address;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Validator;

/**
 * Class CustomValidation
 * @package App\Services
 */
class CustomValidation extends Validator
{
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return mixed
     */
    public function validateAdminLogin($attribute, $value, $parameters)
    {
        return Auth::attempt([
            'email' => trim($parameters[0]),
            'password' => trim($parameters[1]),
            'access_vendorsdb' => 1
        ], Input::get('remember', false) === 'on');
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return mixed
     */
    public function validateDefaultAddressExist($attribute, $value, $parameters)
    {
        $vendorId = $parameters[0];

        if($value == 0) return true;
        return  !Address::where('vendor_id', $vendorId)->where('is_default', 1)->exists();
    }
}