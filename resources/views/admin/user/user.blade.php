@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')

        <div class="widget-box transparent">
            @include('admin.user._userForm')
        </div>
    </div>
@stop