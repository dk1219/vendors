<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends BaseController
{
    public function getIndex()
    {
        if (Auth::check()) {
            return redirect()->route('admin.home');
        }
        return view('admin.login.sign_in');
    }

    public function postSignIn(LoginRequest $request)
    {
        return redirect()->route('admin.home');
    }

    public function getSignOut()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('admin.login.index');
    }
}
