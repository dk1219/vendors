{{-- lead region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if (Route::currentRouteName() === 'admin.vendor.create')
    <h3 class="header smaller lighter green">Create Vendor</h3>
    {!! Form::open(['route' => 'admin.vendor.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_vendor_form'])
    !!}
@else
    @include('admin.vendor._nav')
    <h3 class="header smaller lighter green">Edit Vendor</h3>
    {!! Form::Model($vendor, ['route' => ['admin.vendor.update', $vendor->vendor_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_vendor_form'])
     !!}
@endif

<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Vendor Name:
    </label>
    <div class="col-sm-9">
        {!! Form::text('vendor_name', null, ['class' => 'col-xs-10 col-sm-10',
            'placeholder' =>  trans('validation.required', ['attribute' => 'Vendor Name'])
            ])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Chempax Ref:
    </label>
    <div class="col-sm-9">
        {!! Form::text('chempax_ref', null, ['class' => 'col-xs-10 col-sm-10',
            ])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Website:
    </label>
    <div class="col-sm-9">
        {!! Form::text('website', null, ['class' => 'col-xs-10 col-sm-10',
            ])
        !!}
    </div>
</div>


<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        FD Member:
    </label>
    <div class="col-sm-3">
        {!! Form::select('fd_member', [0=>'NO', 1=>'YES'], null, ['class'=>'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Vendor Type:
    </label>
    <div class="col-sm-3">
        {!! Form::select('vender_type', $vendorTypes, null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Payment Term:
    </label>
    <div class="col-sm-3">
        {!! Form::text('payment_term', null, ['class'=>'col-xs-12 col-sm-12']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Country:
    </label>
    <div class="col-sm-3">
        {!! Form::text('country', null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
</div>
<div class="space-4"></div>

<!---  Field --->
<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Notes:
    </label>
    <div class="col-sm-7">
        {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
    </div>
</div>

@include('admin.vendor._ingredientForm')
<div class="space-20"></div>



<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-info" id="vendor_submit_btn" type="submit">
            @if (Route::currentRouteName() === 'admin.vendor.create')
                Save New Vendor
            @else
                Update Vendor
            @endif
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- lead region end --}}
