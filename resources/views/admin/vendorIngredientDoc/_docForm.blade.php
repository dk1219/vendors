{{-- vendor ingrendient doc region begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if (Route::currentRouteName() === 'admin.vendor-ingredient-doc.create')
    <h3 class="header smaller lighter green">Create Vendor Ingredient Document</h3>
    {!! Form::open(['route' => 'admin.vendor-ingredient-doc.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'enctype'=>'multipart/form-data',
        'id' => 'create_vendor_doc_form'])
    !!}

    <div class="form-group" style="margin-top: 50px;">
        <label for="form-field-1" class="col-sm-2 control-label ">
            Ingredient  Name:
        </label>
        <div class="col-sm-9">
            {!! Form::select('ingredient_id', $ingredientsName, null, ['class'=>'col-xs-10 col-sm-10 chosen', 'id'=>'vendor_ingredient', 'placeholder'=>'Select Ingredient Name']) !!}
        </div>
    </div>
    <div class="space-4"></div>

    <div class="form-group">
        <label for="form-field-1" class="col-sm-2 control-label ">
            Document Type:
        </label>
        <div class="col-sm-9">
            {!! Form::select('doc_type', [], null, ['class'=>'col-xs-10 col-sm-10', 'id'=>'vendor_ingredient_doc_type', 'placeholder'=>'Select Document Type']) !!}
        </div>
    </div>
    <div class="space-4"></div>

@else
    <h3 class="header smaller lighter green">Edit Vendor Ingredient Document</h3>
    {!! Form::Model($vendorIngredientDoc, ['route' => ['admin.vendor-ingredient-doc.update', $vendorIngredientDoc->doc_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'enctype'=>'multipart/form-data',
        'id' => 'edit_vendor_doc_form'])
     !!}

    <div class="form-group" style="margin-top: 50px;">
        <label for="form-field-1" class="col-sm-2 control-label ">
            Ingredient  Name:
        </label>
        <div class="col-sm-9" style="margin-top: 5px;">
            @if(isset($ingredientsName[$vendorIngredientDoc->ingredient_id]))
                {{ $ingredientsName[$vendorIngredientDoc->ingredient_id] }}
            @endif
        </div>
    </div>
    <div class="space-4"></div>

    <div class="form-group">
        <label for="form-field-1" class="col-sm-2 control-label ">
            Document Type:
        </label>
        <div class="col-sm-9" style="margin-top: 5px;">
            @if(isset($docTypes[$vendorIngredientDoc->doc_type]))
                {{ $docTypes[$vendorIngredientDoc->doc_type] }}
            @endif
        </div>
    </div>
    <div class="space-4"></div>
@endif


<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Expire On:
    </label>
    <div class="col-sm-4">
        <div class="input-group">
            <input name="expire_on" value="@if(isset($vendorIngredientDoc->expire_on)) {{$vendorIngredientDoc->expire_on->toDateString()}} @endif"
                   class="form-control date-picker" id="expire_on" type="text" data-date-format="yyyy-mm-dd" />
            <span class="input-group-addon">
                <i class="icon-calendar bigger-110"></i>
            </span>
        </div>
    </div>
</div>
<div class="space-4"></div>


<div class="form-group" >
    <label for="form-field-1" class="col-sm-2 control-label ">
        Document:
    </label>
    <div class="col-sm-9">
        {!! Form::file('doc',['multiple'])!!}
    </div>
</div>
<div class="space-20"></div>

<input type="hidden" name="vendor_id" id="vendor_id" value="{{$vendorId}}">

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-info" id="vendor_doc_submit_btn" type="submit">
            @if (Route::currentRouteName() === 'admin.vendor-ingredient-doc.create')
                Save New Vendor Ingredient Document
            @else
                Update Vendor Ingredient Document
            @endif
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- vendor ingrendient doc end --}}

<script>
    var doc_type_url = "{{ route('admin.vendor_ingredient_doc.doc_type') }}";
</script>
