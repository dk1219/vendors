<?php

namespace App\Jobs;

use App\Consts\VendorType;
use App\Jobs\Job;
use App\Models\Address;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ImportLocalChempaxVendor
 * @package App\Jobs
 */
class ImportLocalChempaxVendor extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var
     */
    public $vendor;

    /**
     * ImportLocalChempaxVendor constructor.
     * @param $vendor
     */
    public function __construct($vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            if(isset($this->vendor) && isset($this->vendor->vendor_name) && !empty($this->vendor->vendor_name)) {
                $this->vendor->vendor_name = trim($this->vendor->vendor_name);
                $existVendor = Vendor::where('vendor_name', '=', $this->vendor->vendor_name)->first();
                if(empty($existVendor)) {
                    $newVendor = [
                        'vendor_name' => $this->vendor->vendor_name,
                        'chempax_ref' => trim($this->vendor->chempax_ref),
                        'country' => 'US',
                        'created_by' => 51,
                        'created_date' => Carbon::now(),
                        'updated_by' => 51,
                        'updated_date' => Carbon::now(),
                        'fd_member' => 0,
                        'active' => 1,
                        'payment_term' => trim($this->vendor->payment_terms),
                    ];

                    if($this->vendor->status === 'F-D') {
                        $newVendor['vender_type'] = VendorType::FACTORY;
                        $newVendor['fd_member'] = 1;
                    } elseif($this->vendor->status === 'M') {
                        $newVendor['vender_type'] = VendorType::FACTORY;
                    } elseif($this->vendor->status === 'M/D') {
                        $newVendor['vender_type'] = VendorType::FACTORY_DISTRIBUTOR;
                    } else {
                        $newVendor['vender_type'] = VendorType::TBD;
                    }

                    $vendor = Vendor::create($newVendor);


                    $existAddress = Address::where('vendor_id', '=', $vendor->vendor_id)
                        ->where('address1', '=', $this->vendor->address_1)->count();

                    if(empty($existAddress)) {
                        $addressToSync[0] = [
                            'address1' => trim( $this->vendor->address_1 ),
                            'address2' => trim( $this->vendor->address_2 ),
                            'city' => trim( $this->vendor->city ),
                            'state' => trim( $this->vendor->state ),
                            'zip' => trim( $this->formatZip( $this->vendor->zip ) ),
                            'phone' => trim( $this->vendor->phone ),
                            'country' => 'US',
                        ];
                        $vendor->Addresses()->createMany($addressToSync);
                    }
                }
            }

        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }

    /**
     * @param $zip
     * @return mixed|string
     */
    protected function formatZip($zip)
    {
        if(empty($zip) || !is_string($zip)) return '';

        $zipArr = explode('-', $zip);
        $zip = collect($zipArr)->first();

        return $zip;
    }
}
