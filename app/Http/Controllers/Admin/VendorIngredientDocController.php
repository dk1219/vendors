<?php

namespace App\Http\Controllers\Admin;

use App\Bll\VendorIngredientDocBll;
use App\Consts\ProdAgreementFileTypes;
use App\Consts\TbName;
use App\Http\Controllers\BaseController;
use App\Models\Ingredient;
use App\Models\Vendor;
use App\Models\VendorIngredientDoc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\VendorIngredientDocRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

/**
 * Class VendorIngredientDocController
 * @package App\Http\Controllers\Admin
 */
class VendorIngredientDocController extends BaseController
{
    /**
     * @var VendorIngredientDoc
     */
    protected $vendorIngredientDoc;

    /**
     * VendorIngredientDocController constructor.
     * @param $vendorIngredientDoc
     */
    public function __construct(VendorIngredientDoc $vendorIngredientDoc)
    {
        parent::__construct();
        $this->sidebar('vendors');
        $this->navName('vendor_ingredient_doc');
        $this->vendorIngredientDoc = $vendorIngredientDoc;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendorId = $request->get('vendor_id');

        if($request->get('filter')) {
            $filter = $request->get('filter');
            Session::put('filter.vendor_ingredient_doc', $filter);
        }elseif(Session::has('filter.vendor_ingredient_doc')){
            $filter = Session::get('filter.vendor_ingredient_doc');
        }else{
            $filter = null;
        }

        $vendorIngredientDocs = VendorIngredientDoc::with('ingredient')->search($filter)->where('vendor_id', $vendorId)
            ->paginate(config('view.paginations.admin'));

        $ingredients = Vendor::findOrFail($vendorId)->ingredients()->orderBy('ingredient_name')->lists('ingredient_name', 'ingredients.ingredient_id');

        return view('admin.vendorIngredientDoc.docList', compact('vendorIngredientDocs', 'vendorId', 'ingredients', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendorId = $request->get('vendor_id');
        if(empty($vendorId)) abort(404);

        $ingredientsName = VendorIngredientDocBll::getIngredientsName($vendorId);

        return view('admin.vendorIngredientDoc.doc', compact('ingredientsName', 'vendorId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendorIngredientDocRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorIngredientDocRequest $request)
    {
        $docType = $request->get('doc_type');
        $vendorId = $request->get('vendor_id');
        $ingredientId = $request->get('ingredient_id');

        if($request->hasFile('doc'))
        {
            $file = $request->file('doc');

            $uploadSucc = VendorIngredientDocBll::uploadDocument($file, $vendorId, $ingredientId, $docType);
            if(!$uploadSucc) {
                return redirect()->route('admin.vendor-ingredient-doc.create', ['vendor_id'=>$vendorId])
                    ->with('fails',
                        trans('notifications.crud.fails.create',
                            [
                                'model' => 'Vendor Ingredient Document'
                            ])
                    );
            }else{
                $this->vendorIngredientDoc->doc_name = $file->getClientOriginalName();
            }
        }

        foreach ($request->only('ingredient_id', 'vendor_id', 'doc_type', 'expire_on') as $key => $value) {
            $this->vendorIngredientDoc->$key = trim($value);
        }
        $this->vendorIngredientDoc->uploaded_by = Auth::User()->admin_user_id;
        $this->vendorIngredientDoc->uploaded_date = Carbon::now();

        $result = $this->vendorIngredientDoc->save();

        if($result) {
            return redirect()->route('admin.vendor-ingredient-doc.index', ['vendor_id'=>$vendorId])
                ->with('success',
                    trans('notifications.crud.success.create',
                        [
                            'model' => 'Vendor Ingredient Document'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-ingredient-doc.create', ['vendor_id'=>$vendorId])
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor Ingredient Document'
                        ])
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $docId
     * @return \Illuminate\Http\Response
     */
    public function edit($docId)
    {
        $vendorIngredientDoc = $this->vendorIngredientDoc->findOrFail($docId);
        $vendorId = $vendorIngredientDoc->vendor_id;
        $ingredientsName = VendorIngredientDocBll::getIngredientsName($vendorId);
        $docTypes = ProdAgreementFileTypes::singleton()->getConsts();

        return view('admin.vendorIngredientDoc.doc', compact('vendorId', 'ingredientsName', 'vendorIngredientDoc', 'docTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendorIngredientDocRequest  $request
     * @param  int  $docId
     * @return \Illuminate\Http\Response
     */
    public function update(VendorIngredientDocRequest $request, $docId)
    {
        $vendorIngredientDoc = $this->vendorIngredientDoc->findOrFail($docId);


        if($request->hasFile('doc'))
        {
            $file = $request->file('doc');

            $uploadSucc = VendorIngredientDocBll::uploadDocument($file, $vendorIngredientDoc->vendor_id, $vendorIngredientDoc->ingredient_id, $vendorIngredientDoc->doc_type);;
            if(!$uploadSucc) {
                return redirect()->route('admin.vendor-ingredient-doc.edit', $docId)
                    ->with('fails',
                        trans('notifications.crud.fails.update',
                            [
                                'model' => 'Vendor Ingredient Document'
                            ])
                    );
            }else{
                $vendorIngredientDoc->doc_name = $file->getClientOriginalName();
            }
        }

        if( !empty( $request->get('expire_on') )  ) $vendorIngredientDoc->expire_on = trim($request->get('expire_on'));

        $vendorIngredientDoc->uploaded_by = Auth::User()->admin_user_id;
        $vendorIngredientDoc->uploaded_date = Carbon::now();

        $result = $vendorIngredientDoc->save();

        if($result) {
            return redirect()->route('admin.vendor-ingredient-doc.index', ['vendor_id'=>$vendorIngredientDoc->vendor_id])
                ->with('success',
                    trans('notifications.crud.success.update',
                        [
                            'model' => 'Vendor Ingredient Document'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-ingredient-doc.edit', $docId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Vendor Ingredient Document'
                        ])
                );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $docId
     * @return \Illuminate\Http\Response
     */
    public function destroy($docId)
    {
        DB::beginTransaction();
        try{
            $vendorIngredientDoc = VendorIngredientDoc::findOrFail($docId);
            $vendorId = $vendorIngredientDoc->vendor_id;
            $ingredientId = $vendorIngredientDoc->ingredient_id;
            $docType = $vendorIngredientDoc->doc_type;
            $docName = $vendorIngredientDoc->doc_name;

            VendorIngredientDocBll::deleteDocS3($vendorId, $ingredientId, $docType, $docName);
            $vendorIngredientDoc->delete();
        }catch(\Exception $e){
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }

    /**
     * @param $docId
     * @return mixed
     */
    public function download($docId)
    {
        if(empty($docId)) abort(404);
        $vendorIngredientDoc = $this->vendorIngredientDoc->findOrFail($docId);
        $vendorId = $vendorIngredientDoc->vendor_id;
        $ingredientId = $vendorIngredientDoc->ingredient_id;
        $docType = $vendorIngredientDoc->doc_type;
        $docName = $vendorIngredientDoc->doc_name;

        return VendorIngredientDocBll::downloadDocument($vendorId, $ingredientId, $docType, $docName);
    }

    /**
     * @param Request $request
     * @return $this|array
     */
    public function getDocTypes(Request $request)
    {
        $ingredientId = $request->get('ingredient_id');
        $vendorId = $request->get('vendor_id');

        if(empty($ingredientId) || empty($vendorId)) return [];

        $docTypes = ProdAgreementFileTypes::singleton()->getConsts();
        $existDocTypes = VendorIngredientDocBll::existDocTypes($vendorId, $ingredientId);
        $docTypes = collect($docTypes)->forget($existDocTypes->flip()->keys()->toArray());

        return $docTypes;

    }
}
