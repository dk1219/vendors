<?php

namespace App\Models;

use App\Consts\TbName;

class Contact extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::VENDOR_CONTACT_TB;


    /**
     * @var string
     */
    public $primaryKey = 'contact_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'title', 'email', 'active', 'address1', 'address2', 'city', 'state',
        'zip', 'tel', 'fax', 'vendor_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'vendor_id');
    }
}
