<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

/**
 * Class VendorQuoteRequest
 * @package App\Http\Requests\Admin
 */
class VendorQuoteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ingredient_id' => 'required',
            'qty' => 'required|numeric',
            'qty_max' => 'numeric',
            'uom' => 'required',
            'unit_price' => 'required|numeric',
            'selling_price' => 'numeric',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'ingredient_id.required' => 'The ingredient name field is required.',
        ];
    }
}
