<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class VendorContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address1' => "required",
            'city' => 'required',
            'state' => 'required',
            'email' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'address1.required' => 'Address is required.',
        ];
    }
}
