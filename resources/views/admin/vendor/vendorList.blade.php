@extends('layouts.default')
@section('content')
    <div class="col-sm-12 widget-container-span profile-tab" id="inv_lead_container">
        @include('partials.errors')
        @include('partials.success')
        @include('partials.fails')
        <div class="header green col-sm-12">
            <h3 class="smaller lighter col-sm-9 lead-title">Vendors List</h3>
            <div class="blue col-sm-3 lead-total">Total Records: <span>{{$vendors->total()}}</span></div>
        </div>

        @include('admin.vendor._search')
        <div class="table">
            <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-1">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th id="vendor_cn_title">Vendor Name</th>
                    <th>Chempax Ref</th>
                    <th>Website</th>
                    <th>FD Member</th>
                    <th style="text-align: center;">Ingredients</th>
                    <th>{{ trans('templates.global.action') }}</th>
                </tr>
                </thead>

                <tbody>
                @if (!$vendors->isEmpty())
                    @foreach ($vendors as $key => $vendor)
                        <tr >
                            <td class="center">
                                <span class="lbl">{{$key+1}}</span>
                            </td>
                            <td>
                                {{ $vendor->vendor_name }}
                            </td>
                            <td>
                                {{ $vendor->chempax_ref }}
                            </td>
                            <td>
                                {{ $vendor->website }}
                            </td>
                            <td>
                                {{ $vendor->fd_member ? 'YES' : 'NO' }}
                            </td>
                            <td style="text-align: center;">
                                @if(!$vendor->Ingredients->isEmpty())
                                    <span class="btn btn-success btn-xs tooltip-success" data-rel="popover" data-placement="left"
                                          data-original-title="<i class='fa fa-leaf' aria-hidden='true'></i> Ingredients"
                                          data-content="@foreach($vendor->Ingredients as $key=>$ingredient)
                                                  <a href={{route('admin.ingredient.edit', $ingredient->ingredient_id)}}>{{$key+1}} - {{$ingredient->ingredient_name}}</a> <br /> @endforeach">
                                        <i class='fa fa-leaf' aria-hidden='true'></i> Ingredients
                                    </span>
                                @endif
                            </td>
                            <td style="width: 150px;">
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.vendor.edit', $vendor) }}">
                                        <i class="icon-edit bigger-120"></i>
                                    </a>
                                    <a class="btn btn-xs btn-danger delete-model" data-url="{{route('admin.vendor.destroy', $vendor)}}">
                                        <i class="icon-trash bigger-120"></i>
                                    </a>

                                    <div class="btn-group" style="margin-left: 0.5px;">
                                        <button data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle">
                                            More
                                            <span class="icon-caret-down icon-on-right"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-info pull-right">
                                            <li>
                                                <a href="{{Route('admin.vendor-address.index', ['vendor_id'=>$vendor])}}">
                                                    Vendor Address
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{Route('admin.vendor-contact.index', ['vendor_id'=>$vendor])}}">
                                                    Vendor Contact
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{Route('admin.vendor-ingredient-doc.index', ['vendor_id'=>$vendor])}}">
                                                    Ingredient's Documents
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{Route('admin.vendor-quote.index', ['vendor_id'=>$vendor])}}">
                                                    Vendor Quote
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="row" style="margin-top: 30px;">
                <div class="col-sm-2 text-center">
                </div>
                <div class="col-sm-10">
                    <div class="dataTables_paginate paging_bootstrap">
                        {!! $vendors->appends(['filter'=>$filter, 'sort_by'=>$sortBy])->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="del_confirm hidden">{{ trans('templates.global.del_confirm') }}</div>
    <div class="del_failed hidden">{{ trans('templates.global.del_failed') }}</div>
    <div class="del_success hidden">{{ trans('templates.global.del_success') }}</div>

@stop