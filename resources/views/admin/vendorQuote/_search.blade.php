<div class="page-header col-sm-12" style="margin-top:19px;">
    {!! Form::open(['route' => ['admin.vendor-quote.index'] ,'method' => 'GET', 'id' => 'vendor_quote_search_form']) !!}
        <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
            @if(!$ingredients->isEmpty())
                {!! Form::select('filter[ingredient_id]', $ingredients, isset($filter['ingredient_id']) ? $filter['ingredient_id'] : '', ['class' => 'search-query pull-left col-xs-5', 'placeholder'=>'Search Ingredient Name']) !!}
            @endif

            <span class="pull-right" style="margin-top: -3px;">
                @if(!$ingredients->isEmpty())
                    <input type="hidden" name="vendor_id" value="{{$vendorId}}">
                    <button type="submit" class="btn btn-purple btn-sm">
                        <i class="icon-search icon-on-right bigger-110"></i>
                        {{ trans('templates.global.search') }}
                    </button>
                @endif
                <a class="btn btn-success btn-sm" href="{{route('admin.vendor-quote.create', ['vendor_id'=>$vendorId])}}">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    Add Quote
                </a>
            </span>
        </div>
    {!! Form::close() !!}
</div>
