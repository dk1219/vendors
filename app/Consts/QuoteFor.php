<?php
/**
 * Created by PhpStorm.
 * User: kongdaniel
 * Date: 9/28/16
 * Time: 10:12
 */

namespace App\Consts;


class QuoteFor extends Consts
{
    const PRICE_MATCH = 1;
    const NEW_SOURCING_INGREDIENTS = 2;
    const FOR_STOCK = 3;
    const OFFLINE_SOURCING_INGREDIENTS = 4;
}