{{-- vendor ingrendient quote begin --}}
<div class="alert alert-danger hidden">
    <strong>
        <div class="validation-errors"></div>
    </strong>
</div>

@if (Route::currentRouteName() === 'admin.vendor-quote.create')
    <h3 class="header smaller lighter green">Create Vendor Quote</h3>
    {!! Form::open(['route' => 'admin.vendor-quote.store',
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_vendor_quote_form'])
    !!}
@else
    <h3 class="header smaller lighter green">Edit Vendor Quote</h3>
    {!! Form::Model($vendorQuote, ['route' => ['admin.vendor-quote.update', $vendorQuote->quote_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_vendor_quote_form'])
     !!}
@endif

<div class="form-group" style="margin-top: 50px;">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Ingredient  Name:
    </label>
    <div class="col-sm-9">
        {!! Form::select('ingredient_id', $ingredientsName, null, ['class'=>'col-xs-8 col-sm-8 chosen', 'placeholder'=>'Select Ingredient Name']) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Quantity:
    </label>
    <div class="col-sm-3">
        {!! Form::text("qty", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Max Qty:
    </label>
    <div class="col-sm-3">
        {!! Form::text("qty_max", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Unit:
    </label>
    <div class="col-sm-3">
        {!! Form::text("uom", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>

    <label for="form-field-1" class="col-sm-2 control-label ">
        Unit Price:
    </label>
    <div class="col-sm-3">
        {!! Form::text("unit_price", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Package:
    </label>
    <div class="col-sm-3">
        {!! Form::select("package_type", $packages, null, ["class" => "col-xs-10 col-sm-10 chosen"]) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Expire On:
    </label>
    <div class="col-sm-3">
        <div class="input-group">
            <input name="expire_on" value="@if(isset($vendorQuote->expire_on)) {{$vendorQuote->expire_on->toDateString()}} @endif"
                   class="form-control date-picker" id="expire_on" type="text" data-date-format="yyyy-mm-dd" />
            <span class="input-group-addon">
                <i class="icon-calendar bigger-110"></i>
            </span>
        </div>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Contact:
    </label>
    <div class="col-sm-8">
        {!! Form::text("contact", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Note:
    </label>
    <div class="col-sm-8">
        {!! Form::text("note", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Quoted For:
    </label>
    <div class="col-sm-3">
        {!! Form::select('quoted_for', $quoteFor, null, ['class'=>'col-xs-10 col-sm-10']) !!}
    </div>
    <label for="form-field-1" class="col-sm-2 control-label ">
        Quote Ref:
    </label>
    <div class="col-sm-3">
        {!! Form::text("quote_ref", null, ["class" => "col-xs-10 col-sm-10"]) !!}
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Ship From:
    </label>
    <div class="col-sm-6">
        {!! Form::text('shipfrom', null, ['class'=>'col-xs-12 col-sm-12']) !!}
    </div>
    <div class="col-sm-2" style="margin-top: 5px;">e.g. zip, city</div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Selling Price:
    </label>
    <div class="col-sm-3">
        {!! Form::text('selling_price', null, ['class'=>'col-xs-12 col-sm-12']) !!}
    </div>
</div>
<div class="space-4"></div>




<div class="space-20"></div>
<input type="hidden" name="vendor_id" id="vendor_id" value="{{$vendorId}}">

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-info" id="vendor_quote_submit_btn" type="submit">
            @if (Route::currentRouteName() === 'admin.vendor-quote.create')
                Submit New Vendor Quote
            @else
                Update Vendor Quote
            @endif
            <i class="icon-ok bigger-110"></i>
        </button>
        <button class="btn btn-warning btn-back" type="button">
            {{ trans('templates.global.back') }}
            <i class="icon-undo bigger-110"></i>
        </button>
    </div>
</div>

{!! Form::close() !!}
{{-- vendor ingrendient quote end --}}