<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ trans('templates.system_title') }}</title>
        <meta name="csrf_token" content="{{ session()->token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300" />
        <link rel="stylesheet" href="{{ elixir('css/all.css') }}">
        <link rel="stylesheet" href="/ace/assets/css/jquery-ui-1.10.3.full.min.css">
        <link rel="stylesheet" href="/ace/assets/css/datepicker.css">
        <link rel="stylesheet" href="/ace/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="/ace/assets/css/ace.min.css">
        <link rel="stylesheet" href="/ace/assets/css/ace-rtl.min.css">
        <link rel="stylesheet" href="/ace/assets/css/ace-skins.min.css">
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
        @yield('css')
    </head>

    <body>
        @include('partials.header')
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try{ace.settings.check('main-container' , 'fixed')}catch(e){}
            </script>

            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>
                @include('partials.left_sidebar')
                <div class="main-content">
                    @yield('breadcrumbs')
                    <div class="page-content">
                        <div class="row">
                            @yield('content')
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->
            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div>
        @include('partials.footer')
        @yield('js_pre')
        <script src="{{ elixir('js/all.js') }}"></script>
        @yield('js')
    </body>
</html>
