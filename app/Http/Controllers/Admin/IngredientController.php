<?php

namespace App\Http\Controllers\Admin;

use App\Consts\TbName;
use App\Http\Controllers\BaseController;
use App\Models\Ingredient;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\IngredientRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * Class IngredientController
 * @package App\Http\Controllers\Admin
 */
class IngredientController extends BaseController
{
    /**
     * @var Ingredient
     */
    protected $ingredient;

    /**
     * IngredientController constructor.
     * @param $ingredient
     */
    public function __construct(Ingredient $ingredient)
    {
        parent::__construct();
        $this->ingredient = $ingredient;
        $this->sidebar('ingredients');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->get('filter')) {
            $filter = $request->get('filter');
            Session::put('filter.ingredient', $filter);
        }elseif(Session::has('filter.ingredient')){
            $filter = Session::get('filter.ingredient');
        }else{
            $filter = null;
        }

        $ingredients = $this->ingredient->where('active', 1)->search($filter)
            ->orderBy('ingredient_name', 'asc')
            ->with('alias','vendors')->paginate(config('view.paginations.admin'));

        return view('admin.ingredient.ingredientList', compact('ingredients', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ingredient.ingredient');
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ingredient_name'=>"required|unique:".TbName::INGREDIENT_TB.",ingredient_name,NULL,id,active,1"
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->except('alias_name'));
        }

        try{
            foreach ($request->only('ingredient_name', 'chempax_ref') as $key => $value) {
                $this->ingredient->$key = trim($value);
            }
            $this->ingredient->save();

            $aliasToSync = [];
            $aliasNames = $request->get('alias_name');
            if($aliasNames) {
                foreach($aliasNames as $key=>$aliasName) {
                    $aliasToSync[$key] = [
                        'alias_name'=>$aliasName,
                    ];
                }
            }
            $this->ingredient->alias()->createMany($aliasToSync);


        }catch(\Exception $e){
            return redirect()->route('admin.ingredient.create')
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Ingredient'
                        ])
                );
        }

        return redirect()->route('admin.ingredient.index')
            ->with('success',
                trans('notifications.crud.success.create',
                    [
                        'model' => 'Ingredient'
                    ])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $ingredientId
     * @return \Illuminate\Http\Response
     */
    public function edit($ingredientId)
    {
        if(empty($ingredientId)) abort(404);

        try{
            $ingredient = $this->ingredient->with('alias', 'vendors')->findOrFail($ingredientId);
        }catch(\Exception $e){
            abort(404);
        }


        return view('admin.ingredient.ingredient',
            compact('ingredient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $ingredientId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ingredientId)
    {
        $validator = Validator::make($request->all(), [
            'ingredient_name'=>"required|unique:".TbName::INGREDIENT_TB.",ingredient_name,".$ingredientId.",ingredient_id,active,1"
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->except('alias_name', 'exist_alias_name'));
        }

        $ingredient = $this->ingredient->findOrFail($ingredientId);

        DB::beginTransaction();
        try{
            foreach ($request->only('ingredient_name', 'chempax_ref') as $key => $value) {
                $ingredient->$key = trim($value);
            }
            $ingredient->save();

            $aliasToSync = [];
            $existAliasNames = $request->get('exist_alias_name');
            $aliasNames = $request->get('alias_name');
            if($existAliasNames) {
                foreach($existAliasNames as $aliasName) {
                    $aliasToSync[] = [
                        'alias_name'=>$aliasName,
                    ];
                }
            }
            if(!empty($aliasNames)) {
                foreach($aliasNames as $aliasName) {
                    $aliasToSync[] = [
                        'alias_name'=>$aliasName,
                    ];
                }
            }
            $ingredient->alias()->delete();
            $ingredient->alias()->createMany($aliasToSync);
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->route('admin.ingredient.edit', $ingredientId)
                ->with('fails',
                    trans('notifications.crud.fails.update',
                        [
                            'model' => 'Ingredient'
                        ])
                );
        }
        DB::commit();

        return redirect()->route('admin.ingredient.index')
            ->with('success',
                trans('notifications.crud.success.update',
                    [
                        'model' => 'Ingredient'
                    ])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $ingredientId
     * @return \Illuminate\Http\Response
     */
    public function destroy($ingredientId)
    {
        $ingredient = $this->ingredient->findOrFail($ingredientId);
        $ingredient->active = 0;
        $ingredient->save();
    }

    /**
     * @param $ingredientId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function related($ingredientId)
    {
        $ingredient = $this->ingredient->with('products', 'products.lead')->findOrFail($ingredientId);

        return view('admin.ingredient.relatedList',
            compact('ingredient'));
    }
}
