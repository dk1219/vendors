<div class="page-header col-sm-12" style="margin-top:19px;">
    {!! Form::open(['route' => ['admin.user.index'] ,'method' => 'GET', 'id' => 'user_search_form']) !!}
        <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
            <input name="filter[fullname]" type="text" value="{{ isset($filter['fullname']) ? $filter['fullname'] : ''}}" class="search-query pull-left col-xs-8"
                   placeholder="{{ trans('templates.global.search_query', ['query' => 'Full Name']) }}" style="margin-right: 15px; height: 34px;">

            <span class="pull-right" style="margin-top: -3px;">
                <a class="btn btn-success btn-sm" href="{{route('admin.user.create')}}">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    Add User
                </a>
            </span>
            <span class="pull-right" style="margin-top: -3px; margin-right: 2px;">
                <button type="submit" class="btn btn-purple btn-sm">
                    <i class="icon-search icon-on-right bigger-110"></i>
                    {{ trans('templates.global.search') }}
                </button>
            </span>
        </div>
    {!! Form::close() !!}
</div>
