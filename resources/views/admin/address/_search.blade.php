<div class="page-header col-sm-12" style="margin-top:19px;">
    <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
        <span class="pull-right" style="margin-top: -3px;">
            <a class="btn btn-success btn-sm" href="{{route('admin.vendor-address.create', ['vendor_id'=>$vendorId])}}">
                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                Add Address
            </a>
        </span>
    </div>
</div>
