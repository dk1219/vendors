<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvSyncTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_sync_tasks', function(Blueprint $table) {
            $table->increments('id')->comment('主键id');
            $table->string('sku', 30)->nullable()->default('')->index('idx_sku')->comment('产品sku');
            $table->string('whs', 30)->nullable()->default('')->index('idx_whs')->comment('仓库id');
            $table->tinyInteger('status')->nullable()->default(1)->comment('任务状态 1-pending 2-finished 3-error ');
            $table->text('result', 65535)->nullable()->comment('任务执行结果');

            $table->integer('created_uid')->unsigned()->nullable()->default(0)->comment('创建者');
            $table->integer('updated_uid')->unsigned()->nullable()->default(0)->comment('更新者');
            $table->timestamps();
            $table->comment = '库存同步任务表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_sync_tasks');
    }
}
