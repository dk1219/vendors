<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Alias;
use App\Models\Ingredient;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ImportVendor
 * @package App\Jobs
 */
class ImportVendor extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue,  SerializesModels;

    /**
     * @var Vendor
     */
    public $vendor;

    /**
     * Create a new job instance.
     * @param Vendor $vendor
     */
    public function __construct($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            if(isset($this->vendor) && isset($this->vendor->vendor_name) && !empty($this->vendor->vendor_name)) {
                $this->vendor->vendor_name = trim($this->vendor->vendor_name);
                $existVendor = Vendor::where('vendor_name', '=', $this->vendor->vendor_name)->first();
                if(empty($existVendor)) {
                    $vendor = Vendor::create([
                        'vendor_name' => $this->vendor->vendor_name,
                        'vender_type' => 1,
                        'created_by' => 24,
                        'created_date' => Carbon::now(),
                        'updated_by' => 24,
                        'updated_date' => Carbon::now(),
                        'fd_member' => 0,
                        'active' => 1
                    ]);
                } else {
                    $vendor = $existVendor;
                }
            }

            if(isset($this->vendor) && isset($this->vendor->ingredient) && !empty($this->vendor->ingredient)) {
                $this->vendor->ingredient = trim($this->vendor->ingredient);

                $existIngredient = Ingredient::where('ingredient_name', '=', $this->vendor->ingredient)->first();

                if(empty($existIngredient)) {
                    $ingredient = Ingredient::create([
                        'ingredient_name' => $this->vendor->ingredient,
                        'active' => 1
                    ]);
                } else {
                    $ingredient = $existIngredient;
                }

                $existIngredientIds = collect( $vendor->Ingredients )->pluck('ingredient_id')->all();

                if(empty($existIngredientIds) || !in_array($ingredient->ingredient_id, $existIngredientIds)) {
                    $ingredientsToSync[$ingredient->ingredient_id] = [
                        'created_by' => 24,
                        'created_date' => Carbon::now(),
                        'updated_by' => 24,
                        'updated_date' => Carbon::now(),
                    ];
                    $vendor->Ingredients()->attach($ingredientsToSync);
                }
            }



            if(isset($this->vendor) && isset($this->vendor->ingredient_alias) && !empty($this->vendor->ingredient_alias)) {
                $this->vendor->ingredient_alias = trim($this->vendor->ingredient_alias);

                $existAlias = Alias::where('ingredient_id', '=', $ingredient->ingredient_id)
                    ->where('alias_name', '=', $this->vendor->ingredient_alias)->count();

                if(empty($existAlias)) {
                    $aliasToSync[0] = [
                        'alias_name'=>$this->vendor->ingredient_alias,
                    ];
                    $ingredient->alias()->createMany($aliasToSync);
                }
            }

        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }
}
