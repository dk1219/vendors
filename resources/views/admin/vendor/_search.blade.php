<div class="page-header col-sm-12" style="margin-top:19px;">
    {!! Form::open(['route' => ['admin.vendor.index'] ,'method' => 'GET', 'id' => 'vendor_search_form']) !!}
        <div class="col-xs-12" style="padding-left: 0px; margin-top: -25px;">
            <input name="filter[vendor_name]" type="text" value="{{ isset($filter['vendor_name']) ? $filter['vendor_name'] : ''}}" class="search-query pull-left col-xs-5"
                   placeholder="{{ trans('templates.global.search_query', ['query' => 'Vendor Name']) }}" style="margin-right: 15px; height: 34px;">

            <span class="pull-right" style="margin-top: -3px;">
                <button type="submit" class="btn btn-purple btn-sm">
                    <i class="icon-search icon-on-right bigger-110"></i>
                    {{ trans('templates.global.search') }}
                </button>
                <a class="btn btn-success btn-sm" href="{{route('admin.vendor.create')}}">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                    Add Vendor
                </a>
                <button type="button" class="btn @if(!$isMoreFilter) btn-purple @else btn-danger @endif btn-sm" id="filter_btn">
                    <i class="icon-search icon-on-right"></i>
                    <span id="filter_text">@if(!$isMoreFilter) More Filter @else Close Filter @endif</span>
                </button>
            </span>
        </div>

        <div class="col-xs-12" style="padding-left: 0px; margin-top: 10px; @if(!$isMoreFilter) display: none; @endif" id="filter_container">
            <input name="filter[website]" type="text" value="{{ isset($filter['website']) ? $filter['website'] : ''}}" class="search-query pull-left col-xs-5"
                   placeholder="{{ trans('templates.global.search_query', ['query' => 'Website']) }}" style="margin-right: 15px; margin-top: 10px; height: 34px;">

            <input name="filter[chempax_ref]" type="text" value="{{ isset($filter['chempax_ref']) ? $filter['chempax_ref'] : ''}}" class="search-query pull-left col-xs-5"
                   placeholder="{{ trans('templates.global.search_query', ['query' => 'Chempax Ref']) }}" style="margin-right: 15px; margin-top: 10px; height: 34px;">
        </div>

    <input type="hidden" name="sort_by" id="lead_sort_by">
    {!! Form::close() !!}
</div>
