<?php

namespace App\Console\Commands;

use App\Models\InvSyncTask;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DelOverdueInvSyncTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'overdue-inv-sync-task:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Overdue Inv sync Task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $overdueDate = Carbon::now()->subDay(14);
        InvSyncTask::where('created_at', '<', $overdueDate)->delete();
    }
}
