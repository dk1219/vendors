<?php

namespace App\Models;

use App\Consts\TbName;

class Alias extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::ALIAS_TB;


    /**
     * @var string
     */
    public $primaryKey = 'alias_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ingredients_id', 'alias_name'
    ];
}
