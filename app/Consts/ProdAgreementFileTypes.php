<?php

namespace App\Consts;


class ProdAgreementFileTypes extends Consts
{
    const ALLERGEN_STATEMENT = 'Allergen Statement';
    const BSE_TSE_STATEMENT = 'BSE TSE Statement';
    const COA = 'COA';
    const COMPOSITION_STATEMENT = 'Composition Statement';
    const CONTINUING_LETTER_GUARANTEE = 'Continuing Letter Guarantee';
    const COUNTRY_OF_ORIGIN_STATEMENT = 'Country of Origin Statement';
    const FDA_STATEMENT = 'FDA Statement';
    const FLOW_CHART = 'Flow Chart';
    const HALAL = 'Halal';
    const KOSHER = 'Kosher';
    const GMP = 'GMP';
    const GRAS = 'GRAS';
    const HTS_STATEMENT = 'HTS Statement';
    const MELAMINE_FREE_STATEMENT = 'Melamine Free Statement';
    const NON_ETO_STATEMENT = 'Non ETO Statement';
    const NON_GMO_STATEMENT = 'Non GMO Statement';
    const NUTRITION_STATEMENT = 'Nutrition Statement';
    const PESTICIDES_STATEMENT = 'Pesticides Statement';
    const PRO65_STATEMENT = 'Pro65 Statement';
    const RESIDUAL_SOLVENT = 'Residual Solvent';
    const SDS = 'SDS';
    const SPEC = 'Spec';
    const VEGETARIAN_STATEMENT = 'Vegetarian Statement';
    const WADA = 'WADA';
}