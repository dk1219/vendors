<?php
/**
 * Vendor Ingredient Doc Business Logic Layer
 * User: kongdaniel
 * Date: 12/19/15
 * Time: 6:41 PM
 */

namespace App\Bll;


use App\Models\Vendor;
use App\Models\VendorIngredientDoc;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

/**
 * Class VendorIngredientDocBll
 * @package App\Bll
 */
class VendorIngredientDocBll extends BaseBll
{
    /**
     * @param $vendorId
     * @return mixed
     */
    public static function getIngredientsName($vendorId)
    {
        return Vendor::with(['ingredients'=>function($query) {
            $query->wherePivot('active', 1)
                ->where('ingredients.active', 1)->orderBy('ingredient_name');
        }])->findOrFail($vendorId)
            ->ingredients->lists('ingredient_name', 'ingredient_id');
    }


    /**
     * @param $file
     * @param $vendorId
     * @param $ingredientId
     * @param $docType
     * @return bool
     */
    public static function uploadDocument($file, $vendorId, $ingredientId, $docType)
    {
        if(!$file->isValid()) return false;

        $directory = $vendorId.'/'.$ingredientId.'/'.$docType;
        $emptyDir = empty(Storage::disk('s3_vi_doc')->allFiles($directory)) ? true : false;

        if(!$emptyDir) {
            Storage::disk('s3_vi_doc')->deleteDirectory($directory);
        }

        $savePath = $directory.'/'.$file->getClientOriginalName();
        $succ = Storage::disk('s3_vi_doc')->put(
            $savePath,
            file_get_contents($file->getRealPath())
        );
        if(!$succ) return false;

        return true;
    }

    /**
     * @param $vendorId
     * @param $ingredientId
     * @return mixed
     */
    public static function existDocTypes($vendorId, $ingredientId)
    {
        return VendorIngredientDoc::where('vendor_id', $vendorId)
            ->where('ingredient_id', $ingredientId)->lists('doc_type');
    }


    /**
     * @param $vendorId
     * @param $ingredientId
     * @param $docType
     * @param $docName
     * @return mixed
     */
    public static function downloadDocument($vendorId, $ingredientId, $docType, $docName)
    {
        $directory = $vendorId.'/'.$ingredientId.'/'.$docType;
        $savePath = $directory.'/'.$docName;

        if(!self::docIsExistS3($savePath)) {
            abort(404);
        }
        $file = Storage::disk('s3_vi_doc')->get($savePath);
        $mimetype = Storage::disk('s3_vi_doc')->mimeType($savePath);
        return (new Response($file, 200))
            ->header('Content-Type', $mimetype)
            ->header('Content-Disposition', 'attachment; filename="'.$docName.'"');
    }

    /**
     * @param $savePath
     * @return bool
     */
    public static function docIsExistS3($savePath)
    {
        return Storage::disk('s3_vi_doc')->has($savePath) ? true : false;
    }


    /**
     * @param $vendorId
     * @param $ingredientId
     * @param $docType
     * @param $docName
     */
    public static function deleteDocS3($vendorId, $ingredientId, $docType, $docName)
    {
        $directory = $vendorId.'/'.$ingredientId.'/'.$docType;
        $savePath = $directory.'/'.$docName;

        if(!self::docIsExistS3($savePath)) {
            abort(404);
        }

        Storage::disk('s3_vi_doc')->delete($savePath);
    }
}