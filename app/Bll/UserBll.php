<?php
/**
 * User Business Logic Layer
 * User: kongdaniel
 * Date: 12/19/15
 * Time: 6:41 PM
 */

namespace App\Bll;

use App\Models\User;

class UserBll extends BaseBll
{
    /**
     *  Get user list
     *
     */
    public static function getUserList()
    {
        return User::orderBy('fullname', 'asc')->lists('admin_user_id', 'fullname');
    }

}