<?php

namespace App\Http\Controllers\Admin;

use App\Bll\VendorQuoteDocBll;
use App\Http\Controllers\BaseController;
use App\Models\QuoteDoc;
use App\Models\VendorQuote;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\VendorQuoteDocRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class VendorQuoteDocController
 * @package App\Http\Controllers\Admin
 */
class VendorQuoteDocController extends BaseController
{
    /**
     * @var QuoteDoc
     */
    protected $vendorQuoteDoc;

    /**
     * VendorQuoteDocController constructor.
     * @param $vendorQuoteDoc
     */
    public function __construct(QuoteDoc $vendorQuoteDoc)
    {
        parent::__construct();
        $this->sidebar('vendors');
        $this->navName('vendor_quote');
        $this->vendorQuoteDoc = $vendorQuoteDoc;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $quoteId = $request->get('quote_id');
        $vendorQuoteDocs = QuoteDoc::where('quote_id', $quoteId)
            ->paginate(config('view.paginations.admin'));
        $vendorId = VendorQuote::where('quote_id', $quoteId)->pluck('vendor_id');

        return view('admin.vendorQuoteDoc.docList', compact('vendorQuoteDocs', 'vendorId', 'quoteId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $quoteId = $request->get('quote_id');
        $vendorId = $request->get('vendor_id');
        if(empty($quoteId) || empty($vendorId)) abort(404);

        return view('admin.vendorQuoteDoc.doc', compact('quoteId', 'vendorId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendorQuoteDocRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorQuoteDocRequest $request)
    {
        $quoteId = $request->get('quote_id');
        $vendorId = $request->get('vendor_id');

        $file = $request->file('doc');

        $savePath = $vendorId.'/'.$quoteId.'/'.$file->getClientOriginalName();
        $isExist = VendorQuoteDocBll::docIsExistS3($savePath);
        if($isExist) return redirect()->back()->withErrors(['Filename already exists.']);

        $uploadSucc = VendorQuoteDocBll::uploadDocument($file, $vendorId, $quoteId);
        if(!$uploadSucc) {
            return redirect()->route('admin.vendor-quote-doc.create', ['vendor_id'=>$vendorId, 'quote_id'=>$quoteId])
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor Ingredient Document'
                        ])
                );
        }else{
            $this->vendorQuoteDoc->file_path = $vendorId.'/'.$quoteId;
            $this->vendorQuoteDoc->doc_name = $file->getClientOriginalName();
        }

        $this->vendorQuoteDoc->quote_id = $quoteId;

        $result = $this->vendorQuoteDoc->save();

        if($result) {
            return redirect()->route('admin.vendor-quote-doc.index', ['quote_id'=>$quoteId])
                ->with('success',
                    trans('notifications.crud.success.create',
                        [
                            'model' => 'Vendor Quote Document'
                        ])
                );
        }else{
            return redirect()->route('admin.vendor-quote-doc.create', ['vendor_id'=>$vendorId, 'quote_id'=>$quoteId])
                ->with('fails',
                    trans('notifications.crud.fails.create',
                        [
                            'model' => 'Vendor Quote Document'
                        ])
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $docId
     * @return \Illuminate\Http\Response
     */
    public function destroy($docId)
    {
        DB::beginTransaction();
        try{
            $vendorQuoteDoc = QuoteDoc::with('VendorQuote')->findOrFail($docId);
            $vendorId = $vendorQuoteDoc->VendorQuote->vendor_id;
            $quoteId = $vendorQuoteDoc->quote_id;
            $docName = $vendorQuoteDoc->doc_name;

            VendorQuoteDocBll::deleteDocS3($vendorId, $quoteId, $docName);
            $vendorQuoteDoc->delete();
        }catch(\Exception $e){
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }

    /**
     * @param $docId
     * @return mixed
     */
    public function download($docId)
    {
        if(empty($docId)) abort(404);
        $vendorQuoteDoc = $this->vendorQuoteDoc->with('VendorQuote')->findOrFail($docId);
        $vendorId = $vendorQuoteDoc->VendorQuote->vendor_id;
        $quoteId = $vendorQuoteDoc->quote_id;
        $docName = $vendorQuoteDoc->doc_name;

        return VendorQuoteDocBll::downloadDocument($vendorId, $quoteId, $docName);
    }
}
