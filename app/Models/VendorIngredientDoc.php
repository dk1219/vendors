<?php

namespace App\Models;


use App\Consts\TbName;

/**
 * Class VendorIngredientDoc
 * @package App\Models
 */
class VendorIngredientDoc extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::VENDOR_INGREDIENT_DOC_TB;


    /**
     * @var string
     */
    public $primaryKey = 'doc_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_ingredient_id', 'doc_name', 'doc_file', 'doc_type', 'uploaded_by', 'uploaded_date', 'expire_on',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'uploaded_date', 'expire_on'
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'vendor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Ingredient()
    {
        return $this->belongsTo(Ingredient::class, 'ingredient_id', 'ingredient_id');
    }

    /**
     * Vendor Ingredient Doc filter search
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeSearch($query, $filter)
    {
        if (empty($filter)) {
            return $query;
        }

        foreach($filter as $key => $value) {
            if (empty(trim($value))) {
                continue;
            }
            $value = trim($value);

            if($key=='ingredient_id') {
                $query->where(function($query) use($value){
                    $query->where($this->getTable().'.ingredient_id', '=', $value);
                });
            }
        }

        return $query;
    }
}
