<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\PullInvSyncTasks::class,
        \App\Console\Commands\HandleInvSyncTasks::class,
        \App\Console\Commands\DelOverdueInvSyncTasks::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('inv-sync-task:pull')
//            ->cron('*/3 * * * *');

//        $schedule->command('inv-sync-task:handle')
//            ->cron('*/5 * * * *');
//
//        $schedule->command('overdue-inv-sync-task:delete')
//            ->daily();
    }
}
