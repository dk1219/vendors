<?php
/**
 * 常量和枚举类数据定义
 */

namespace App\Consts;

abstract class Consts implements \ArrayAccess
{

    protected $consts;
    protected static $instance;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->consts = (new \ReflectionClass($this))->getConstants();
    }

    /**
     * 单例对象
     * @return static
     */
    public static function singleton()
    {
        return new static;
    }

    /**
     * 验证是否存在
     * @param $val
     * @return bool
     */
    public static function valid($val)
    {
        $self = static::singleton();
        return in_array($val, $self);
    }

    /**
     * 转成字符串
     * @return string
     */
    public static function toString()
    {
        $self = static::singleton();
        return implode(',', $self->getConsts());
    }

    /**
     * 转成数组
     * @return mixed
     */
    public static function toArray()
    {
        $self = static::singleton();
        return $self->getConsts();
    }

    /**
     * 获取常量列表
     * @return array
     */
    public function getConsts()
    {
        return $this->consts;
    }

    //魔术方法
    public function offsetSet($offset, $value)
    {
        if ( !is_null($offset) ) {
            $this->consts[$offset] = $value;
        }
    }

    //魔术方法
    public function offsetExists($offset)
    {
        return isset($this->consts[$offset]);
    }

    //魔术方法
    public function offsetUnset($offset)
    {
        unset($this->consts[$offset]);
    }

    //魔术方法
    public function offsetGet($offset)
    {
        return isset($this->consts[$offset]) ? $this->consts[$offset] : null;
    }
}