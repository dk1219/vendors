<?php

namespace App\Models;

use App\Consts\TbName;
use Illuminate\Database\Eloquent\Model;

class Dict extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::DICT_TB;

    /**
     * The databease connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['list_name', 'list_array'];

    /**
     * Get the user's first name.
     *
     * @param  string  $value
     * @return string
     */
    public function getListArrayAttribute($value)
    {
        if(empty($value)) return [];
        $listArr = explode(',', $value);
        $formatedList = [];
        foreach($listArr as $val) {
            $val = trim($val);
            $formatedList[$val] = $val;
        }
        return $formatedList;
    }
}
