<?php
/**
 * Vendor Quote Business Logic Layer
 * User: kongdaniel
 */

namespace App\Bll;


use App\Consts\QuoteFor;
use App\Models\Package;
use App\Models\Vendor;
use App\Models\VendorIngredientDoc;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class VendorQuoteBll
 * @package App\Bll
 */
class VendorQuoteBll extends BaseBll
{
    /**
     * @param $vendorId
     * @return mixed
     */
    public static function getIngredientsName($vendorId)
    {
        return Vendor::with(['ingredients'=>function($query) {
            $query->wherePivot('active', 1)
                ->where('ingredients.active', 1)->orderBy('ingredient_name');
        }])->findOrFail($vendorId)
            ->ingredients->lists('ingredient_name', 'ingredient_id');
    }

    /**
     * @return array
     */
    public static function getQuoteFor()
    {
        return [
            QuoteFor::FOR_STOCK => 'Stork',
            QuoteFor::NEW_SOURCING_INGREDIENTS => 'New Sourcing Ingredients',
            QuoteFor::OFFLINE_SOURCING_INGREDIENTS => 'Offline Sourcing Ingredients',
            QuoteFor::PRICE_MATCH => 'Price Match',
        ];
    }

    /**
     * @param $vendorId
     * @param $quoteId
     */
    public static function deleteDocFolderS3($vendorId, $quoteId)
    {
        $savePath = $vendorId.'/'.$quoteId;

        if(Storage::disk('s3_vq_doc')->has($savePath)) {
            Storage::disk('s3_vq_doc')->deleteDirectory($savePath);
        }
    }

    /**
     * @return mixed
     */
    public static function getPackageTypes()
    {
        DB::select( DB::raw("SET ANSI_NULLS ON") );
        DB::select( DB::raw("SET ANSI_WARNINGS ON") );
        $packages = Package::orderBy('Description', 'asc')->lists('Description', 'Packaging-code');

        return $packages;
    }
}