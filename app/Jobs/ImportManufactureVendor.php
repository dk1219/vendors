<?php

namespace App\Jobs;

use App\Consts\VendorType;
use App\Jobs\Job;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportManufactureVendor extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $vendor;

    /**
     * ImportManufactureVendor constructor.
     * @param $vendor
     */
    public function __construct($vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            if(isset($this->vendor) && isset($this->vendor->vendor_name) && !empty($this->vendor->vendor_name)) {
                $this->vendor->vendor_name = trim($this->vendor->vendor_name);
                $existVendor = Vendor::where('vendor_name', '=', $this->vendor->vendor_name)->first();
                if(empty($existVendor)) {
                    $newVendor = [
                        'vendor_name' => $this->vendor->vendor_name,
                        'country' => 'US',
                        'created_by' => 51,
                        'created_date' => Carbon::now(),
                        'updated_by' => 51,
                        'updated_date' => Carbon::now(),
                        'fd_member' => 0,
                        'active' => 1,
                        'website' => trim($this->vendor->website),
                        'notes' => trim($this->vendor->notes),
                    ];

                    $this->vendor->vendor_type = strtolower( trim($this->vendor->vendor_type) );

                    if($this->vendor->vendor_type === 'mfg') {
                        $newVendor['vender_type'] = VendorType::FACTORY;
                    } elseif($this->vendor->vendor_type === 'mfg/broker') {
                        $newVendor['vender_type'] = VendorType::FACTORY_BROKER;
                    } else {
                        $newVendor['vender_type'] = VendorType::TBD;
                    }

                    Vendor::create($newVendor);
                }
            }

        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
        DB::commit();
    }
}
