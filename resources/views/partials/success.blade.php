@if (Session::has('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">
                <i class="icon-remove"></i>
        </button>
        <strong>
            {{ Session::get('success') }}
        </strong>
    </div>
@endif

