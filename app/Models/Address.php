<?php

namespace App\Models;

use App\Consts\TbName;

/**
 * Class Address
 * @package App\Models
 */
class Address extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::ADDRESS_TB;


    /**
     * @var string
     */
    public $primaryKey = 'address_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id', 'shipfrom_flag', 'billto_flag', 'is_default', 'active', 'address1', 'address2',
        'city', 'state', 'zip', 'country', 'first_name', 'last_name', 'phone', 'email'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'vendor_id');
    }
}
