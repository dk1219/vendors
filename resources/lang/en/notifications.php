<?php
return [
    'crud' => [
        'success' => [
            'create' => ':model Create Successful!',
            'update' => ':model Update Successful!',
            'delete' => ':model Delete Successful!',
            'sync'   => ':model Sync Successful',
            'generate'   => ':model Gemerate Successful！',
        ],
        'fails' => [
            'create' => ':model Create Failed!',
            'update' => ':model Update Failed!',
            'delete' => ':model Delete Failed!',
            'exists' => ':model Has Exists!',
            'sync'   => ':model Sync Failed',
            'generate'   => ':model Generate Failed！',
            'no_term' => 'Term is Expired or Can not be Found',
            'empty_doc_prefix' => 'Doc Prefix is empty, please ask IT to complete it first!'
        ]
    ]
];
