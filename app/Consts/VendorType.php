<?php

namespace App\Consts;


class VendorType extends Consts
{
    const FACTORY = 1;
    const DISTRIBUTOR = 2;
    const BROKER = 3;
    const COMPETITOR = 4;
    const FACTORY_DISTRIBUTOR = 5;
    const FACTORY_BROKER = 6;
    const IMPORTER = 7;
    const IMPORTER_BROKER = 8;
    const TBD = 9;
}