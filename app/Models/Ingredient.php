<?php

namespace App\Models;


use App\Consts\TbName;

/**
 * Class Ingredient
 * @package App\Models
 */
class Ingredient extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = TbName::INGREDIENT_TB;


    /**
     * @var string
     */
    public $primaryKey = 'ingredient_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'chempax_ref', 'ingredient_name', 'active'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Vendors()
    {
        return $this->belongsToMany(Vendor::class, 'vendor_ingredients', 'ingredient_id', 'vendor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Alias()
    {
        return $this->hasMany(Alias::class, 'ingredient_id', 'ingredient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function VendorIngredientDocs()
    {
        return $this->hasMany(VendorIngredientDoc::class, 'ingredient_id', 'ingredient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function VendorQuotes()
    {
        return $this->hasMany(VendorQuote::class, 'ingredient_id', 'ingredient_id');
    }

    /**
     * Ingredient filter search
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeSearch($query, $filter)
    {
        if (empty($filter)) {
            return $query;
        }

        foreach($filter as $key => $value) {
            if (empty(trim($value))) {
                continue;
            }
            $value = trim($value);

            if($key=='ingredient_name') {
                $query->where($this->getTable().'.ingredient_name', 'like', $value.'%');
            }
            if($key=='chempax_ref') {
                $query->whereRaw($this->getTable().'.chempax_ref=?', [$value]);
            }
            if($key=='no_chempax') {
                if($value=='Y') {
                    $query->where(function($query){
                        $query->whereNull($this->getTable().'.chempax_ref')
                            ->orWhere($this->getTable().'.chempax_ref', '=', '');
                    });
                }
            }
        }

        return $query;
    }
}
