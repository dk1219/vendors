@if(isset($vendor) && !$vendor->ingredients->isEmpty())
    <h3 class="header smaller lighter green">
        Exists Ingredients
    </h3>
    <div id="exist_ingredients_container">
        @foreach($vendor->ingredients as $key=>$ingredient)
            <div class="form-group">
                <label for="form-field-1" class="col-sm-2 control-label ">
                    Ingredient {{$key+1}}:
                </label>
                <div class="col-sm-6">
                    {!! Form::text("exist_ingredient_name[]", $ingredient->ingredient_name, ["class" => "col-xs-10 col-sm-10", "disabled"]) !!}
                    {!! Form::text("ingredient_id[]", $ingredient->ingredient_id, ["class" => "col-xs-10 col-sm-10 hidden"]) !!}
                </div>
                <a class="btn btn-success btn-xs" style="margin-top: 1px;" href="{{ route('admin.vendor-ingredient-doc.index', ['vendor_id'=>$vendor, 'filter[ingredient_id]'=>$ingredient]) }}">Ingredient's Documents</a>
                <button class="btn btn-danger btn-xs remove_ingredient_btn" style="margin-top: 1px;" type="button" ingredient-id="{{$ingredient->ingredient_id}}" remove-url="{{ route('admin.vendor.check_docs', [$vendor, $ingredient]) }}">Remove</button>
            </div>
        @endforeach
    </div>
@endif


<h3 class="header smaller lighter green" style="margin-top: 50px;">
    Add Ingredient
</h3>
<div class="form-group">
    <label for="form-field-1" class="col-sm-2 control-label ">
        Ingredient:
    </label>
    <div class="col-sm-8">
        {!! Form::select("new_ingredient", $allIngredients, null, ["id"=>"new_ingredient", "class" => "col-xs-8 col-sm-8 chosen", 'placeholder'=>'-- ingredients --']) !!}
        <button class="btn btn-info btn-xs" id="add_ingredient_btn" style="margin-left: 10px;" type="button">
            <i class="fa fa-plus-circle" aria-hidden="true"></i>
            Add Ingredient
        </button>
    </div>
</div>

<table class="table table-striped table-bordered table-hover dataTable col-xs-9" id="new_ingredients_container" style="margin-bottom: 50px; margin-left: 10%; width: 60%; display: none;">
    <thead>
    <tr>
        <th class="center">Ingredient</th>
        <th style="width: 50px;">Action</th>
    </tr>
    </thead>

    <tbody>
    </tbody>
</table>
