<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

/**
 * Class BaseController
 * @package App\Http\Controllers\Suppliers
 */
class BaseController extends Controller
{

    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        //
    }


    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param string $errno
     * @return mixed
     */
    public function responseNotFound($errno = 'not_found')
    {
        return $this->setStatusCode(404)->responseError($errno);
    }

    /**
     * @param $errno
     * @return mixed
     */
    protected function responseError($errno)
    {
        $errmsg = trans('error.'.$errno);
        if(!$errmsg) $errmsg = trans('error.unknown_error');
        return $this->response([
            'status' => 'failed',
            'status_code' => $this->getStatusCode(),
            'msg' => $errmsg
        ]);
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function responseSuccess($data, $succno='')
    {
        $succmsg = trans('success.'.$succno);
        if(!$succmsg) $succmsg = trans('success.success');
        return $this->response([
            'status' => 'success',
            'status_code' => $this->getStatusCode(),
            'msg'   => $succmsg,
            'obj' => $data
        ]);
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function response($data)
    {
        return Response::json($data, 200);
    }

    /**
     * success return json
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($succno, $data=null)
    {
        $succmsg = trans('success.'.$succno);
        if(!$succmsg) $succmsg = trans('success.success');
        return response()->json([
            'success'   => true,
            'msg'   => $succmsg,
            'data'      => $data
        ]);
    }

    /**
     * error return json
     * @param $errno
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function error($errno,$data=null)
    {
        $errmsg = trans('error.'.$errno);
        if(!$errmsg) $errmsg = trans('error.unknown_error');
        return response()->json([
            'success'   => false,
            'errno'     => $errno ,
            'msg'    => $errmsg,
            'data'      => $data
        ]);
    }

    /**
     * set currently active menu
     * @param string $menu      parent menu
     * @param string $subMenu   submenu
     * @return void
     */
    public function sidebar($menu,$subMenu='')
    {
        view()->share('sidebar', ['menu' => $menu, 'subMenu'=> $subMenu]);
    }

    /**
     * set currently nav name
     *
     * @param $name
     */
    public function navName($name)
    {
        view()->share('nav_name', $name);
    }

    /**
     * Get picture real url
     *
     * @param $picUrl
     * @return string
     */
    public function real_img_url($picUrl)
    {
        return asset('images/'.$picUrl);
    }

    /**
     * Get picture real path
     *
     * @param $picUrl
     * @return string
     */
    public function real_img_path($picUrl)
    {
        return public_path('images/'.$picUrl);
    }
}
